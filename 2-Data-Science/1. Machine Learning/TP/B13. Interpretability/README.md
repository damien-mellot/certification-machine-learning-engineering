# TD - Intelligibilité

## Structure du td
```bash
td_intelligibilite
├── data
│   ├── carInsurance_test.csv                                         # test dataset
│   ├── carInsurance_train.csv                                        # train dataset
│   ├── DSS_DMC_Description.pdf                                       # dataset description
│   └── howtogetdata.txt                                              # how to get the datasets
├── notebooks
│   ├── [Obligatoire] Data School - Intelligibilité [Exercice]        # exercises on intelligibility
│   └── Solutions
│       └── [Obligatoire] Data School - Intelligibilité [Correction]  # solution
├── utils                                                             # utils functions
│   ├── __init__.py
│   ├── create_car_data.py
│   ├── mltask.py
│   ├── plot.py
│   └── postprocessing.py
├── activate.sh                                                       # activate the virtual environment
├── init.sh                                                           # initialize the virtual environment
├── README.md                                                         # README.md (the file you're reading)
└── requirements.txt                                                  # python packages needed for exercises
```

## Créer son environnement virtuel

Pour commencer à travailler sur les exercices, commencez par initialiser l'environnement virtuel avec la commande `source init.sh`.
L'environnement virtuel va se créer puis va installer les packages nécessaires pour les exercices.

## Désactiver/Activer son environnement virtuel

Pour désactiver l'environnement virtuel, il suffit de lancer la commande `deactivate`. Pour l'activer il suffit de lancer la commande `source activate.sh`.

## Faire les exercices

Vous pourrez donc lancer la commande `jupyter notebook` puis travailler dans le répertoire `notebooks` où se situent à la fois les exercices et les solutions.
