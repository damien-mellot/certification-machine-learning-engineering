import spacy

nlp_fr = spacy.load('fr')
doc = nlp_fr('Apple cherche à racheter une startup américaine pour 1 million de dollars')

for ent in doc.ents:
    print(ent.text, ent.start_char, ent.end_char, ent.label_)