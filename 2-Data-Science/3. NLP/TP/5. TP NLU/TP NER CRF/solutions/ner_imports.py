# USEFUL FOR pipreqs
from itertools import chain

import nltk
import sklearn
import scipy.stats
from sklearn.metrics import make_scorer
from sklearn.cross_validation import cross_val_score
from sklearn.grid_search import RandomizedSearchCV
import names
import dialgoflow

import sklearn_crfsuite
from sklearn_crfsuite import scorers
from sklearn_crfsuite import metrics