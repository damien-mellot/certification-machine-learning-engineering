def word2features(sentence, word_index):
    """ Transforms a word into a set of features

    Parameters
    ----------
    sentence: [(word, PoS tag, entity)]
        Raw sentence
    word_index: int
        Word index

    Returns
    -------
    features: {}
        Dictionary of features
    """

    word = sentence[word_index][0]
    postag = sentence[word_index][1]

    features = {
        'bias': 1.0,
        'word.lower()': word.lower(),
        'word[-3:]': word[-3:],
        'word[-2:]': word[-2:],
        'word.isupper()': word.isupper(),
        'word.istitle()': word.istitle(),
        'word.isdigit()': word.isdigit(),
        'postag': postag,
        'postag[:2]': postag[:2],
    }
    if word_index > 0:
        # Predecessor
        word1 = sentence[word_index-1][0]
        postag1 = sentence[word_index-1][1]
        features.update({
            '-1:word.lower()': word1.lower(),
            '-1:word.istitle()': word1.istitle(),
            '-1:word.isupper()': word1.isupper(),
            '-1:postag': postag1,
            '-1:postag[:2]': postag1[:2],
        })
    else:
        features['BOS'] = True

    if word_index < len(sentence)-1:
        # Successor
        word1 = sentence[word_index+1][0]
        postag1 = sentence[word_index+1][1]
        features.update({
            '+1:word.lower()': word1.lower(),
            '+1:word.istitle()': word1.istitle(),
            '+1:word.isupper()': word1.isupper(),
            '+1:postag': postag1,
            '+1:postag[:2]': postag1[:2],
        })
    else:
        features['EOS'] = True

    return features
