crf = sklearn_crfsuite.CRF(
    algorithm='lbfgs', 
    max_iterations=100, 
    all_possible_transitions=True
)
params_space = {
    'c1': expon(scale=0.5),
    'c2': expon(scale=0.05)
}

f1_scorer = make_scorer(flat_f1_score, 
                        average='weighted', labels=labels)

# search
rs = RandomizedSearchCV(crf, params_space, 
                        cv=3, 
                        verbose=1, 
                        n_jobs=-1, 
                        n_iter=20, 
                        scoring=f1_scorer)
rs.fit(X_train, y_train)