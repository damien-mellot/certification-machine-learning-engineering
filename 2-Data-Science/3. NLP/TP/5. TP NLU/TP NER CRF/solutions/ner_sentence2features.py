def sentence2features(sentence):
    """ Transforms a sentence into a set of features

    Parameters
    ----------
    sentence: [(word, PoS tag, entity)]
        Raw sentence
    word_index: int
        Word index

    Returns
    -------
    features: {}
        Dictionary of features
    """
    return [word2features(sentence, i) for i in range(len(sentence))]
