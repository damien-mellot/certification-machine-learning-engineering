X_train = [sentence2features(sentence) for sentence in train_sentences]
y_train = [sentence2labels(sentence) for sentence in train_sentences]

X_test = [sentence2features(sentence) for sentence in test_sentences]
y_test = [sentence2labels(sentence) for sentence in test_sentences]