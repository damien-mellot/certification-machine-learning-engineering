# La plupart des entités présentes dans le dataset sont des "O",
# i.e. des entités qui ne nous intéressent pas, car elles n'ont aucune valeur métier.
# Le dataset est donc fortement non balancé en faveur d'entités inintéréssantes.
# Une façon de prendre en compte ce déséquilibre est d'utiliser une mesure d'évaluation pondérée, 
# comme par exemple le F1-score moyenné.