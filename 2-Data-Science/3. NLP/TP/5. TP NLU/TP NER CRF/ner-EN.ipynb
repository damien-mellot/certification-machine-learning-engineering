{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img src=\"images/logo.jpeg\" alt=\"Smiley face\" height=\"100\">\n",
    "<h1>NLP Training - Chatbot & Natural Language Understanding</h1>\n",
    "<h2>Named Entity Recognition</h2>\n",
    "\n",
    "Author : [Antoine ISNARDY](aisnardy@quantmetry.com)\n",
    "\n",
    "**Agenda**\n",
    "1. [Using pre-trained models](#pretrained)\n",
    "3. [Training a CRF](#crf)\n",
    "    - [Formalization](#form)\n",
    "    - [Data](#data)\n",
    "    - [Features](#features)\n",
    "    - [Training](#train)\n",
    "    - [Evaluation](#eval)\n",
    "    - [Optimization](#optim)\n",
    "    - [Results](#results)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---\n",
    "\n",
    "The construction of a chatbot relies on \"Natural Language Understanding\".\n",
    "\n",
    "NLU is a task whose objective is to:\n",
    "\n",
    "- Identify a user's **intent**, such as the desire to obtain the weather\n",
    "- Identify the **parameters** of this intention (slot), such as people, dates, etc.\n",
    "\n",
    "One way to identify the intention is to perform a logistic regression; this is not covered here.\n",
    "\n",
    "Once this intention has been identified, its parameters must be identified; in other words, it is about detecting named entities within the sentence, i.e. concepts that have meaning. **It is on this extraction of named entities that this tutorial focuses.**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id='pretrained'></a>\n",
    "# 1. Utilization of pre-trained models\n",
    "\n",
    "A first way to proceed, **extremely comfortable**, is to use turnkey solutions, such as the now very famous [spaCy](https://spacy.io/) library .\n",
    "\n",
    "Advantages :\n",
    "- Quick to get started\n",
    "- Decent performance on standard entities\n",
    "\n",
    "Disadvantages :\n",
    "- Relative performance in French\n",
    "- Does not work on specific entities"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Google 5 11 ORG\n",
      "\n",
      " 70 71 MISC\n",
      "Siri 92 96 MISC\n",
      "Apple 99 104 ORG\n",
      "iPhone 121 127 MISC\n",
      "Alexa d'Amazon 132 146 ORG\n",
      "\n",
      " 147 148 ORG\n",
      "Echo 180 184 PER\n",
      "Dot 188 191 PER\n"
     ]
    }
   ],
   "source": [
    "import spacy\n",
    "\n",
    "text = \"\"\"Mais Google commence en retard. La compagnie a fait une entrée tardive\n",
    "dand le hardware, et Siri d'Apple, disponible sur iPhone, et Alexa d'Amazon,\n",
    "qui s'exécute sur ses appareils Echo et Dot, sont clairement devant en termes d'adoption client.\"\"\"\n",
    "\n",
    "nlp_fr = spacy.load('fr')\n",
    "doc = nlp_fr(text)\n",
    "\n",
    "for ent in doc.ents:\n",
    "    print(ent.text, ent.start_char, ent.end_char, ent.label_)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Google 4 10 ORG\n",
      "\n",
      " 64 65 GPE\n",
      "Apple 84 89 ORG\n",
      "iPhones 111 118 PRODUCT\n",
      "Amazon 124 130 ORG\n",
      "Alexa\n",
      " 133 139 ORG\n",
      "Echo 167 171 GPE\n",
      "Dot 176 179 ORG\n",
      "\n",
      " 208 209 GPE\n"
     ]
    }
   ],
   "source": [
    "text = \"\"\"But Google is starting from behind. The company made a late push\n",
    "into hardware, and Apple’s Siri, available on iPhones, and Amazon’s Alexa\n",
    "software, which runs on its Echo and Dot devices, have clear leads in\n",
    "consumer adoption.\"\"\"\n",
    "\n",
    "nlp_en = spacy.load('en')\n",
    "doc = nlp_en(text)\n",
    "\n",
    "for ent in doc.ents:\n",
    "    print(ent.text, ent.start_char, ent.end_char, ent.label_)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**This is it!** . Using pre-trained models is as simple as that.\n",
    "\n",
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id='crf'></a>\n",
    "# 2. Training a CRF"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "While the pre-trained models are extremely simple to get started with, they have one major limitation: they do not work on specific entities. As soon as a concept deviates from the standard concepts, it becomes impossible to use a pre-trained model.\n",
    "\n",
    "It is then imperative to train your own NER engine.\n",
    "\n",
    "Several alternatives are possible:\n",
    "- Use the [spaCy](https://spacy.io/usage/linguistic-features#section-named-entities) engine to specialize it on specific entities; this is a relatively black box operation, although one of the bricks has a CRF\n",
    "- Training your own CRF: this is the exercise conducted here"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id='form'></a>\n",
    "\n",
    "## 2.1 Mathematical formalization of the problem\n",
    "\n",
    "CRFs are used for structured prediction problems. The \"structured\" aspect comes from the fact that the input data is a text; as such, the order of words matters and cannot (must not) be ignored.\n",
    "\n",
    "**A few notations:**\n",
    "- $X = (x_1, ..., ..., x_n)$ is a sentence composed of $n$ words\n",
    "- $Y = (y_1, ...,..., y_n)$ is the sequence of the corresponding entities (Person, Place,...)\n",
    "- $x_i=(x_{i1},...,x_{iD})^T \\in \\mathbb{R}^D$ is the characteristic vector of the $i$-th word in the sentence (previous word, next word, isNumeric,...)\n",
    "- $\\mathcal{D} =  \\Big\\{\\Big(X^{(1)}, Y^{(1)}\\Big), ..., \\Big(X^{(N)}, Y^{(N)}\\Big)\\Big\\}$ is the training data, iid.\n",
    "\n",
    "The objective is then to find $\\hat y$ such that:\n",
    "$$\\hat Y = \\arg\\max_{Y \\in \\mathcal{Y}}p(Y/X)$$\n",
    "\n",
    "The parametric discriminant model of CRFs is written as follows:\n",
    "$$\\begin{align*}p(Y/X, \\theta) &= \\frac{1}{Z(X, \\theta)} \\exp \\sum_{j=1}^D\\theta_j F_j(X, Y) \\\\ &= \\frac{1}{Z(X, \\theta)} \\exp \\psi(X, Y, \\theta)\\end{align*}$$\n",
    "With:\n",
    "- $Z(X, \\theta) = \\sum_{Y} \\exp \\sum_j \\theta_j F_j(X, Y)$ a partition function, $F$ being a characteristic function of the data\n",
    "- $\\psi(X, Y,\\theta)=\\sum_j\\theta_jF_j(X, Y)$ a potential function\n",
    "\n",
    "**Intuitions:**\n",
    "1. Estimate of $\\theta$ : maximization of maximum likelihood by gradient descent (or any more sophisticated technique)\n",
    "2. Inference of $\\hat y$, $Z$ and $p(Y/X)$ : Viterbi algorithm to find the most likely sequence of states"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id='data'></a>\n",
    "## 2.2 Data : CoNNL 2002\n",
    "The data used in this TP are in **Spanish**\n",
    "\n",
    "Note: it is extremely **expensive** to obtain labelled NER data."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "import nltk # nltk is a standard text analysis library"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Training and test data**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "[nltk_data] Downloading package conll2002 to\n",
      "[nltk_data]     /Users/gmocquet/nltk_data...\n",
      "[nltk_data]   Unzipping corpora/conll2002.zip.\n"
     ]
    },
    {
     "data": {
      "text/plain": [
       "True"
      ]
     },
     "execution_count": 4,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# Download the data at 'corpora/conll2002.zip/conll2002/'\n",
    "nltk.download('conll2002')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "CPU times: user 2.71 s, sys: 128 ms, total: 2.84 s\n",
      "Wall time: 2.9 s\n"
     ]
    }
   ],
   "source": [
    "%%time\n",
    "train_sentences = list(nltk.corpus.conll2002.iob_sents('esp.train'))\n",
    "test_sentences = list(nltk.corpus.conll2002.iob_sents('esp.testb'))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[('Melbourne', 'NP', 'B-LOC'),\n",
       " ('(', 'Fpa', 'O'),\n",
       " ('Australia', 'NP', 'B-LOC'),\n",
       " (')', 'Fpt', 'O'),\n",
       " (',', 'Fc', 'O'),\n",
       " ('25', 'Z', 'O'),\n",
       " ('may', 'NC', 'O'),\n",
       " ('(', 'Fpa', 'O'),\n",
       " ('EFE', 'NC', 'B-ORG'),\n",
       " (')', 'Fpt', 'O'),\n",
       " ('.', 'Fp', 'O')]"
      ]
     },
     "execution_count": 6,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "train_sentences[0]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "At this point, we have $X$ and $Y$, although they are not yet formatted correctly, as for example, we have not created any feature."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id='features'></a>\n",
    "## 2.3 Features\n",
    "\n",
    "A sentence is represented by a set of $n$ words $X = (x_1, ..., x_n)$. It remains to define each $x_i \\in \\in \\mathbb{R}^D$ : it is feature engineering.\n",
    "\n",
    "Unlike a time series where the future is unknown, a sentence is \"immediately\" fully available.\n",
    "\n",
    "This means that it is possible to use all the words in the sentence to build the features."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Exercise - Feature engineering of a word**. Create a function that takes as input a sentence and the index of a word, and that creates a set of features for this word:\n",
    "- This word in lowercase\n",
    "- Is the word a number?\n",
    "- Does the word have a capital letter?\n",
    "- The following word\n",
    "- The previous word\n",
    "- ... All you can imagine\n",
    "\n",
    "*Constraint: we build a linear CRF. This means that a word can only be linked to its predecessor and successor. Note: it is legal to use the following word, since the sentence is already fully available (unlike for example a stock market where the future is not available)*"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [],
   "source": [
    "def word2features(sentence, word_index):\n",
    "    \"\"\" Transforms a word into a set of features\n",
    "    \n",
    "    Parameters\n",
    "    ----------\n",
    "    sentence: [(word, PoS tag, entity)]\n",
    "        Raw sentence\n",
    "    word_index: int\n",
    "        Word index\n",
    "        \n",
    "    Returns\n",
    "    -------\n",
    "    features: {}\n",
    "        Dictionary of features\n",
    "    \"\"\"\n",
    "    word = sentence[word_index][0]\n",
    "    postag = sentence[word_index][1]\n",
    "    \n",
    "    features = {\n",
    "        'word.lower()': word.lower()\n",
    "    }\n",
    "    \n",
    "    # TODO : add features\n",
    "    \n",
    "    return features"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [],
   "source": [
    "# %load solutions/ner_word2features.py\n",
    "def word2features(sentence, word_index):\n",
    "    \"\"\" Transforms a word into a set of features\n",
    "\n",
    "    Parameters\n",
    "    ----------\n",
    "    sentence: [(word, PoS tag, entity)]\n",
    "        Raw sentence\n",
    "    word_index: int\n",
    "        Word index\n",
    "\n",
    "    Returns\n",
    "    -------\n",
    "    features: {}\n",
    "        Dictionary of features\n",
    "    \"\"\"\n",
    "\n",
    "    word = sentence[word_index][0]\n",
    "    postag = sentence[word_index][1]\n",
    "\n",
    "    features = {\n",
    "        'bias': 1.0,\n",
    "        'word.lower()': word.lower(),\n",
    "        'word[-3:]': word[-3:],\n",
    "        'word[-2:]': word[-2:],\n",
    "        'word.isupper()': word.isupper(),\n",
    "        'word.istitle()': word.istitle(),\n",
    "        'word.isdigit()': word.isdigit(),\n",
    "        'postag': postag,\n",
    "        'postag[:2]': postag[:2],\n",
    "    }\n",
    "    if word_index > 0:\n",
    "        # Predecessor\n",
    "        word1 = sentence[word_index-1][0]\n",
    "        postag1 = sentence[word_index-1][1]\n",
    "        features.update({\n",
    "            '-1:word.lower()': word1.lower(),\n",
    "            '-1:word.istitle()': word1.istitle(),\n",
    "            '-1:word.isupper()': word1.isupper(),\n",
    "            '-1:postag': postag1,\n",
    "            '-1:postag[:2]': postag1[:2],\n",
    "        })\n",
    "    else:\n",
    "        features['BOS'] = True\n",
    "\n",
    "    if word_index < len(sentence)-1:\n",
    "        # Successor\n",
    "        word1 = sentence[word_index+1][0]\n",
    "        postag1 = sentence[word_index+1][1]\n",
    "        features.update({\n",
    "            '+1:word.lower()': word1.lower(),\n",
    "            '+1:word.istitle()': word1.istitle(),\n",
    "            '+1:word.isupper()': word1.isupper(),\n",
    "            '+1:postag': postag1,\n",
    "            '+1:postag[:2]': postag1[:2],\n",
    "        })\n",
    "    else:\n",
    "        features['EOS'] = True\n",
    "\n",
    "    return features\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "{'bias': 1.0,\n",
       " 'word.lower()': 'melbourne',\n",
       " 'word[-3:]': 'rne',\n",
       " 'word[-2:]': 'ne',\n",
       " 'word.isupper()': False,\n",
       " 'word.istitle()': True,\n",
       " 'word.isdigit()': False,\n",
       " 'postag': 'NP',\n",
       " 'postag[:2]': 'NP',\n",
       " 'BOS': True,\n",
       " '+1:word.lower()': '(',\n",
       " '+1:word.istitle()': False,\n",
       " '+1:word.isupper()': False,\n",
       " '+1:postag': 'Fpa',\n",
       " '+1:postag[:2]': 'Fp'}"
      ]
     },
     "execution_count": 9,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "word2features(train_sentences[0], 0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Exercise - Feature engineering of a sentence**. Create a function that takes a sentence as input, and creates the feature set for that sentence"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [],
   "source": [
    "# %load solutions/ner_sentence2features.py\n",
    "def sentence2features(sentence):\n",
    "    \"\"\" Transforms a sentence into a set of features\n",
    "\n",
    "    Parameters\n",
    "    ----------\n",
    "    sentence: [(word, PoS tag, entity)]\n",
    "        Raw sentence\n",
    "    word_index: int\n",
    "        Word index\n",
    "\n",
    "    Returns\n",
    "    -------\n",
    "    features: {}\n",
    "        Dictionary of features\n",
    "    \"\"\"\n",
    "    return [word2features(sentence, i) for i in range(len(sentence))]\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "{'bias': 1.0,\n",
       " 'word.lower()': 'melbourne',\n",
       " 'word[-3:]': 'rne',\n",
       " 'word[-2:]': 'ne',\n",
       " 'word.isupper()': False,\n",
       " 'word.istitle()': True,\n",
       " 'word.isdigit()': False,\n",
       " 'postag': 'NP',\n",
       " 'postag[:2]': 'NP',\n",
       " 'BOS': True,\n",
       " '+1:word.lower()': '(',\n",
       " '+1:word.istitle()': False,\n",
       " '+1:word.isupper()': False,\n",
       " '+1:postag': 'Fpa',\n",
       " '+1:postag[:2]': 'Fp'}"
      ]
     },
     "execution_count": 11,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "sentence2features(train_sentences[0])[0]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Exercise - Extraction of labels (entities)**. Create a function that takes a sentence as input, and creates the sequence of associated labels/entities"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [],
   "source": [
    "def sentence2labels(sentence):\n",
    "    \"\"\" Extracts sequence of entities\n",
    "    \n",
    "    Parameters\n",
    "    ----------\n",
    "    sentence: [(word, PoS tag, entity)]\n",
    "        Raw sentence\n",
    "        \n",
    "    Returns\n",
    "    -------\n",
    "    sequence_of_entities: [entitiy]\n",
    "        List of entities\n",
    "    \"\"\"\n",
    "    sequence_of_entities = []\n",
    "    return sequence_of_entities"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [],
   "source": [
    "# %load solutions/ner_sentence2labels.py\n",
    "def sentence2labels(sentence):\n",
    "    \"\"\" Extracts sequence of entities\n",
    "\n",
    "    Parameters\n",
    "    ----------\n",
    "    sentence: [(word, PoS tag, entity)]\n",
    "        Raw sentence\n",
    "\n",
    "    Returns\n",
    "    -------\n",
    "    sequence_of_entities: [entitiy]\n",
    "        List of entities\n",
    "    \"\"\"\n",
    "    return [entity for _, _, entity in sentence]\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "['B-LOC', 'O', 'B-LOC', 'O', 'O', 'O', 'O', 'O', 'B-ORG', 'O', 'O']"
      ]
     },
     "execution_count": 14,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "sentence2labels(train_sentences[0])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Exercise - Transform the data.** Apply feature engineering to training and test datasets"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {},
   "outputs": [],
   "source": [
    "X_train = []\n",
    "y_train = []\n",
    "\n",
    "X_test = []\n",
    "y_test = []"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "metadata": {},
   "outputs": [],
   "source": [
    "# %load solutions/ner_create_dataset.py\n",
    "X_train = [sentence2features(sentence) for sentence in train_sentences]\n",
    "y_train = [sentence2labels(sentence) for sentence in train_sentences]\n",
    "\n",
    "X_test = [sentence2features(sentence) for sentence in test_sentences]\n",
    "y_test = [sentence2labels(sentence) for sentence in test_sentences]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id='train'></a>\n",
    "## 2.4 CRF training\n",
    "To train the CRF, the [sklearn-crfsuite](https://sklearn-crfsuite.readthedocs.io/en/latest/index.html) package is used."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "metadata": {},
   "outputs": [],
   "source": [
    "import sklearn_crfsuite"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Exercise - Train the CRF.** Use the sklearn-crfsuite API, which is sklearn-compatible, to train the CRF. Use the optimization algorithm `lbfgs`, and an elastic-net penalty, i.e. `l1` and `l2`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "metadata": {},
   "outputs": [],
   "source": [
    "# TODO"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "CRF(algorithm='lbfgs', all_possible_states=None,\n",
       "  all_possible_transitions=True, averaging=None, c=None, c1=0.1, c2=0.1,\n",
       "  calibration_candidates=None, calibration_eta=None,\n",
       "  calibration_max_trials=None, calibration_rate=None,\n",
       "  calibration_samples=None, delta=None, epsilon=None, error_sensitive=None,\n",
       "  gamma=None, keep_tempfiles=None, linesearch=None, max_iterations=100,\n",
       "  max_linesearch=None, min_freq=None, model_filename=None,\n",
       "  num_memories=None, pa_type=None, period=None, trainer_cls=None,\n",
       "  variance=None, verbose=False)"
      ]
     },
     "execution_count": 19,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# %load solutions/ner_train_crf.py\n",
    "crf = sklearn_crfsuite.CRF(\n",
    "    algorithm='lbfgs',\n",
    "    c1=0.1,\n",
    "    c2=0.1,\n",
    "    max_iterations=100,\n",
    "    all_possible_transitions=True\n",
    ")\n",
    "crf.fit(X_train, y_train)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id='eval'></a>\n",
    "## 2.5 Evaluation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Question:** What major (and common in data science) problem does the data suffer from?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 20,
   "metadata": {},
   "outputs": [],
   "source": [
    "# %load solutions/en_ner_eval_answers.py\n",
    "# Most of the entities present in the dataset are \"O\",\n",
    "# i.e. entities that do not interest us, because they have no business value.\n",
    "# The dataset is therefore strongly unbalanced in favor of uninterested entities.\n",
    "# One way to take this imbalance into account is to use a weighted evaluation measure, \n",
    "# such as the average F1-score."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 21,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "['B-LOC', 'B-ORG', 'B-PER', 'I-PER', 'B-MISC', 'I-ORG', 'I-LOC', 'I-MISC']"
      ]
     },
     "execution_count": 21,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "labels = list(crf.classes_)\n",
    "labels.remove('O')\n",
    "labels"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Exercise - Performance calculation.** Calculate the overall `f1 score`, do not forget to average. Tip: read the doc of the function"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 22,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn_crfsuite.metrics import flat_f1_score"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 23,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "0"
      ]
     },
     "execution_count": 23,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "f1_score = 0 # TODO\n",
    "f1_score"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 24,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "0.7964686316443963"
      ]
     },
     "execution_count": 24,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# %load solutions/ner_eval.py\n",
    "y_pred = crf.predict(X_test)\n",
    "flat_f1_score(y_test, y_pred, \n",
    "            average='weighted', labels=labels)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Exercise - Results by class**. Calculate the results by class"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 25,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn_crfsuite.metrics import flat_classification_report"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 26,
   "metadata": {},
   "outputs": [],
   "source": [
    "# TODO"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 27,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "              precision    recall  f1-score   support\n",
      "\n",
      "       B-LOC      0.810     0.784     0.797      1084\n",
      "       I-LOC      0.690     0.637     0.662       325\n",
      "      B-MISC      0.731     0.569     0.640       339\n",
      "      I-MISC      0.699     0.589     0.639       557\n",
      "       B-ORG      0.807     0.832     0.820      1400\n",
      "       I-ORG      0.852     0.786     0.818      1104\n",
      "       B-PER      0.850     0.884     0.867       735\n",
      "       I-PER      0.893     0.943     0.917       634\n",
      "\n",
      "   micro avg      0.813     0.787     0.799      6178\n",
      "   macro avg      0.791     0.753     0.770      6178\n",
      "weighted avg      0.809     0.787     0.796      6178\n",
      "\n"
     ]
    }
   ],
   "source": [
    "# %load solutions/ner_eval_class.py\n",
    "# group B and I results\n",
    "sorted_labels = sorted(\n",
    "    labels, \n",
    "    key=lambda name: (name[1:], name[0])\n",
    ")\n",
    "print(flat_classification_report(\n",
    "    y_test, y_pred, labels=sorted_labels, digits=3\n",
    "))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id='optim'></a>\n",
    "## 2.6 Optimization\n",
    "\n",
    "The model has so far been trained in a hazardous way, i.e. with arbitrary values of `c1` and `c2`. This section proposes to choose the \"best\" set of parameters, using cross-validation."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 28,
   "metadata": {},
   "outputs": [],
   "source": [
    "from scipy.stats import expon\n",
    "from sklearn.model_selection import RandomizedSearchCV\n",
    "from sklearn_crfsuite.scorers import make_scorer"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Exercise - Finding the \"best combination\" of parameters:**\n",
    "- Define the classifier\n",
    "- Define the range of `c1` and `c2` using `expon`.\n",
    "- Define a scoring function useful for training via `make_scorer` and `flat_f1_score`.\n",
    "- Train models with `RandomizedSearchCV` by setting the number of explorations to twenty and `n_jobs=-1` for more speed"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 29,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Fitting 3 folds for each of 20 candidates, totalling 60 fits\n"
     ]
    },
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "[Parallel(n_jobs=-1)]: Using backend LokyBackend with 4 concurrent workers.\n",
      "/Users/gmocquet/.local/share/virtualenvs/notebooks-9LyWe8nh/lib/python3.7/site-packages/sklearn/externals/joblib/externals/loky/process_executor.py:706: UserWarning: A worker stopped while some jobs were given to the executor. This can be caused by a too short worker timeout or by a memory leak.\n",
      "  \"timeout or by a memory leak.\", UserWarning\n",
      "[Parallel(n_jobs=-1)]: Done  42 tasks      | elapsed: 22.2min\n",
      "[Parallel(n_jobs=-1)]: Done  60 out of  60 | elapsed: 30.3min finished\n"
     ]
    },
    {
     "data": {
      "text/plain": [
       "RandomizedSearchCV(cv=3, error_score='raise-deprecating',\n",
       "          estimator=CRF(algorithm='lbfgs', all_possible_states=None,\n",
       "  all_possible_transitions=True, averaging=None, c=None, c1=None, c2=None,\n",
       "  calibration_candidates=None, calibration_eta=None,\n",
       "  calibration_max_trials=None, calibration_rate=None,\n",
       "  calibration_samples=None, delta=None, epsilon=None, error...e,\n",
       "  num_memories=None, pa_type=None, period=None, trainer_cls=None,\n",
       "  variance=None, verbose=False),\n",
       "          fit_params=None, iid='warn', n_iter=20, n_jobs=-1,\n",
       "          param_distributions={'c1': <scipy.stats._distn_infrastructure.rv_frozen object at 0x13b1d1668>, 'c2': <scipy.stats._distn_infrastructure.rv_frozen object at 0x13b1d1828>},\n",
       "          pre_dispatch='2*n_jobs', random_state=None, refit=True,\n",
       "          return_train_score='warn',\n",
       "          scoring=make_scorer(flat_f1_score, average=weighted, labels=['B-LOC', 'B-ORG', 'B-PER', 'I-PER', 'B-MISC', 'I-ORG', 'I-LOC', 'I-MISC']),\n",
       "          verbose=1)"
      ]
     },
     "execution_count": 29,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# %load solutions/ner_optim.py\n",
    "crf = sklearn_crfsuite.CRF(\n",
    "    algorithm='lbfgs', \n",
    "    max_iterations=100, \n",
    "    all_possible_transitions=True\n",
    ")\n",
    "params_space = {\n",
    "    'c1': expon(scale=0.5),\n",
    "    'c2': expon(scale=0.05)\n",
    "}\n",
    "\n",
    "f1_scorer = make_scorer(flat_f1_score, \n",
    "                        average='weighted', labels=labels)\n",
    "\n",
    "# search\n",
    "rs = RandomizedSearchCV(crf, params_space, \n",
    "                        cv=3, \n",
    "                        verbose=1, \n",
    "                        n_jobs=-1, \n",
    "                        n_iter=20, \n",
    "                        scoring=f1_scorer)\n",
    "rs.fit(X_train, y_train)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Visualization of best results"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 30,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "best params: {'c1': 0.08059271925926932, 'c2': 0.03638109425338872}\n",
      "best CV score: 0.7492490637216613\n",
      "model size: 1.50M\n"
     ]
    }
   ],
   "source": [
    "print('best params:', rs.best_params_)\n",
    "print('best CV score:', rs.best_score_)\n",
    "print('model size: {:0.2f}M'.format(rs.best_estimator_.size_ / 1000000))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id='results'></a>\n",
    "## 2.7 Results\n",
    "What does the CRF learn? What transitions?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 31,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Transitions les plus probables:\n",
      "B-ORG  -> I-ORG   7.687744\n",
      "I-ORG  -> I-ORG   7.302677\n",
      "B-MISC -> I-MISC  7.042175\n",
      "I-MISC -> I-MISC  6.834598\n",
      "B-PER  -> I-PER   6.832600\n",
      "B-LOC  -> I-LOC   5.856711\n",
      "I-LOC  -> I-LOC   5.112263\n",
      "I-PER  -> I-PER   5.105184\n",
      "O      -> O       4.375808\n",
      "O      -> B-ORG   2.948498\n",
      "O      -> B-PER   2.752887\n",
      "O      -> B-LOC   2.145943\n",
      "O      -> B-MISC  1.796684\n",
      "B-ORG  -> O       0.561051\n",
      "B-MISC -> B-ORG   0.234281\n",
      "B-LOC  -> B-LOC   0.199065\n",
      "B-ORG  -> B-LOC   0.168545\n",
      "I-PER  -> B-LOC   0.158391\n",
      "B-MISC -> O       0.045793\n",
      "B-LOC  -> O       -0.238724\n",
      "\n",
      "Transitions les plus improbables:\n",
      "B-ORG  -> B-ORG   -2.610431\n",
      "I-MISC -> I-PER   -2.616678\n",
      "I-ORG  -> I-PER   -2.637734\n",
      "I-MISC -> I-ORG   -2.648881\n",
      "I-LOC  -> B-MISC  -2.732002\n",
      "I-PER  -> I-LOC   -2.792485\n",
      "I-MISC -> B-LOC   -2.872475\n",
      "B-PER  -> B-MISC  -2.974603\n",
      "I-PER  -> B-ORG   -3.054426\n",
      "I-ORG  -> I-LOC   -3.178796\n",
      "B-ORG  -> B-MISC  -3.183422\n",
      "I-ORG  -> B-MISC  -3.194798\n",
      "B-PER  -> B-PER   -3.393543\n",
      "I-MISC -> I-LOC   -3.491505\n",
      "B-MISC -> B-MISC  -3.654822\n",
      "I-PER  -> B-MISC  -3.762363\n",
      "O      -> I-MISC  -5.696201\n",
      "O      -> I-ORG   -5.995127\n",
      "O      -> I-PER   -6.254311\n",
      "O      -> I-LOC   -6.911624\n"
     ]
    }
   ],
   "source": [
    "from collections import Counter\n",
    "\n",
    "crf = rs.best_estimator_\n",
    "\n",
    "def print_transitions(trans_features):\n",
    "    for (label_from, label_to), weight in trans_features:\n",
    "        print(\"%-6s -> %-7s %0.6f\" % (label_from, label_to, weight))\n",
    "\n",
    "print(\"Transitions les plus probables:\")\n",
    "print_transitions(Counter(crf.transition_features_).most_common(20))\n",
    "\n",
    "print(\"\\nTransitions les plus improbables:\")\n",
    "print_transitions(Counter(crf.transition_features_).most_common()[-20:])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
