# Bienvenue
Bienvenue sur le support de formation Chatbot & Natural Language Understanding.
Ce repository contient notamment les ressources suivantes:
- Le TP sur la détection d'entités nommées : `ner.ipynb`
- Le TP sur Dialogflow : `dialogflow.ipynb`

# Prérequis
- Disposer d'un compte Google pour pouvoir utiliser https://dialogflow.com

# Installation
- Ouvrir un terminal
- `cd path/to/directory`
- Suivre les instructions disponilbes à https://dialogflow-python-client-v2.readthedocs.io/en/latest/
- `export GOOGLE_APPLICATION_CREDENTIALS="Maif-xxx.json"`
- `pip install -r requirements.txt`
- `python -m spacy download en`
- `python -m spacy download fr`
- `python -m spacy download es`

# Utilisation
- `jupyter notebook`
