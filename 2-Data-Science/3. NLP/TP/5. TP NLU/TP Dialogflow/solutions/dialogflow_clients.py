clients = pd.DataFrame(columns=['name', 'uid', 'date_of_birth', 'day_month', 'sex', 'location'])

list_of_names = [names.get_full_name(gender='female') for _ in range(int(n_clients/2))]
list_of_names += [names.get_full_name(gender='male') for _ in range(int(n_clients / 2))]
clients['name'] = list_of_names

clients['uid'] = clients.index

dates = pd.date_range('01/01/1930', '01/01/1998')
dates_of_birth = list(dates[np.random.randint(0, len(dates), size=n_clients)])
clients['date_of_birth'] = dates_of_birth
clients['day_month'] = clients['date_of_birth'].apply(lambda x: str(x.day) + '-' + str(x.month))

clients.loc[:int(n_clients/2), 'sex'] = 'F'
clients.loc[int(n_clients/2):, 'sex'] = 'M'

clients['location'] = np.random.choice(locations, size=n_clients, replace=True)

clients.head()
