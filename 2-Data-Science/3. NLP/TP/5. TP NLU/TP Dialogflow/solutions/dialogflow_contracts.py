contracts = pd.DataFrame(columns=['uid', 'cid', 'price', 'type'])

contracts['uid'] = np.random.randint(0, n_clients, size=n_contracts)

contracts['cid'] = contracts.index

contracts['price'] = np.random.randint(0, 100000, size=n_contracts)

contracts['type'] = np.random.choice(contract_types, size=n_contracts, replace=True)

contracts.head()
