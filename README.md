# certification-machine-learning-engineering

**Yotta Academy** est la nouvelle solution de formation proposée par Quantmetry à haute valeur-ajoutée que tous les data enthousiastes ont rêvé d’avoir !

Le MLE Bootcamp est dédié à la formation au métier de Machine Learning Engineer et n’a aucun équivalent sur le marché !

LA SEULE FORMATION POUR DEVENIR MACHINE LEARNING ENGINEER

Un programme d’excellence qui te permettra d’accéder au job le plus prisé par les entreprises Data, Tech & IA!

- +500h d’enseignements combinant différents formats théoriques, pratiques et projets

- 100% des modules enseignés par des ingénieurs, docteurs et experts spécialisés en IA

- 4 mois, 16 semaines, temps plein (4 octobre 2020 - 29 janvier 2021)

- 3 projets complets : *Machine Learning*, *Natural Language Processing*, et *Mise en production sur Google Cloud Platform*

