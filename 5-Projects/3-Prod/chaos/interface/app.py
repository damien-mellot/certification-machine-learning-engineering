import streamlit as st
import pandas as pd
import requests
import json
import chaos.settings.base as stg
from io import StringIO

API_URL = "http://pythonapi:8000"


def create_dataframe_with_predictions(df, predictions):
    df_pred = pd.DataFrame.from_dict(
        predictions.get("prediction"), orient="index", columns=["Prediction"]
    )
    df_pred.index = df_pred.index.map(int)
    return pd.concat([df, df_pred], axis=1)


def highlight_survived(s):
    return ["color: green"] * len(s) if s.Prediction > 0.8 else ["color: red"] * len(s)


def project_description():
    """Streamlit widget to show the project description on the home page."""
    # Text/Title
    st.title("Predict Subscription to Financial Product")
    # Image
    st.image(
        "https://mms.businesswire.com/media/20160517006477/en/525395/5/P4317Q.jpg",
        width=700,
        caption=" ",
    )
    # Header/Subheader
    st.header("By **Emilio de Sousa** and **Damien Mellot**")
    st.header("Yotta Academy, 2020-2021")


def decode_bytes_to_df(bytes_data):
    s = str(bytes_data, "utf-8")
    data = StringIO(s)
    return pd.read_csv(data, sep=stg.SEP)


def get_prediction(csv_file_buffer):
    bytes_data = csv_file_buffer.read()
    df = decode_bytes_to_df(bytes_data)
    _files = {"file": bytes_data}
    url = f"{API_URL}/multiples_predict"
    response = requests.post(url, _files)
    if response.status_code == 200:
        predictions = json.loads(response.text)
        new_df = create_dataframe_with_predictions(df, predictions)
        st.dataframe(new_df.style.apply(highlight_survived, axis=1))
        st.balloons()
    else:
        st.error("Bad CSV File, please take the one with marketing datas")


project_description()
csv_file_buffer = st.file_uploader("Upload an image", type=["csv"])

if st.button("Predict"):
    if csv_file_buffer is not None:
        get_prediction(csv_file_buffer)
    else:
        st.error("Please upload a file")


example = {
    "DATE": "2020-03-31",
    "AGE": 50,
    "JOB_TYPE": "Technicien",
    "STATUS": "Marié",
    "EDUCATION": "Secondaire",
    "HAS_DEFAULT": "No",
    "BALANCE": 234,
    "HAS_HOUSING_LOAN": "No",
    "HAS_PERSO_LOAN": "No",
    "CONTACT": "Portable",
    "DURATION_CONTACT": 299,
    "NB_CONTACT": 1,
    "NB_DAY_LAST_CONTACT": -1,
    "NB_CONTACT_LAST_CAMPAIGN": 0,
    "RESULT_LAST_CAMPAIGN": "",
}

if st.button("Predict Example"):
    url = f"{API_URL}/predict"
    try:
        response = requests.post(url, json=example)
        response.raise_for_status()
    except requests.exceptions.ConnectionError as errc:
        st.error("The API is not open")
        st.stop()
    predictions = json.loads(response.text)
    df = pd.DataFrame([example])
    new_df = create_dataframe_with_predictions(df, predictions)
    st.dataframe(new_df.style.apply(highlight_survived, axis=1))
    st.balloons()
