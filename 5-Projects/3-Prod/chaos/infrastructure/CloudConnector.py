from google.cloud import storage
import os
import chaos.settings as stg


class GCPConnector:
    def __init__(self, bucket_name):
        """Initialize gcp connector

        Parameters
        ----------
        bucket_name : str
            bucket name in google cloud storage
        """
        self.bucket_name = bucket_name

    def download_blobs(self, dict_source_dest):
        """Download objets in google cloud storage for a specified bucket

        Parameters
        ----------
        dict_source_dest : Dict
            dict with file to downloads
            key => name of object in GCP
            value => destination path in local
        """
        for source, dest in dict_source_dest.items():
            self._download_blob(source, dest)

    def _download_blob(self, source_blob_name, destination_file_name):

        storage_client = storage.Client()
        bucket = storage_client.bucket(self.bucket_name)
        if os.path.isfile(destination_file_name):
            print(f"File {source_blob_name} already exists")
        else:
            blob = bucket.blob(source_blob_name)
            blob.download_to_filename(destination_file_name)
            print(f"Blob {source_blob_name} downloaded to {destination_file_name}.")


# if __name__ == "__main__":
#    connector = GCPConnector("chaos-6")
#    connector.download_blobs({"pipe.pkl": os.path.join(stg.DATA_DIR, "pipe.pkl")})
