from starlette.testclient import TestClient
from chaos.application.api_main import app
import chaos.settings as stg
from chaos.domain.customer import Customer
from chaos.application.api import predict
import os
from pathlib import Path
import csv
import pandas as pd

client = TestClient(app)

data_test = [
    {
        "DATE": "2020-03-31",
        "AGE": 50,
        "JOB_TYPE": "Technicien",
        "STATUS": "Marié",
        "EDUCATION": "Secondaire",
        "HAS_DEFAULT": "No",
        "BALANCE": 234,
        "HAS_HOUSING_LOAN": "No",
        "HAS_PERSO_LOAN": "No",
        "CONTACT": "Portable",
        "DURATION_CONTACT": 299,
        "NB_CONTACT": 1,
        "NB_DAY_LAST_CONTACT": -1,
        "NB_CONTACT_LAST_CAMPAIGN": 0,
        "RESULT_LAST_CAMPAIGN": "",
    },
    {
        "DATE": "2020-04-30",
        "AGE": 12,
        "JOB_TYPE": "Manager",
        "STATUS": "Marié",
        "EDUCATION": "Tertiaire",
        "HAS_DEFAULT": "No",
        "BALANCE": 234,
        "HAS_HOUSING_LOAN": "No",
        "HAS_PERSO_LOAN": "No",
        "CONTACT": "Portable",
        "DURATION_CONTACT": 299,
        "NB_CONTACT": 1,
        "NB_DAY_LAST_CONTACT": -1,
        "NB_CONTACT_LAST_CAMPAIGN": 0,
        "RESULT_LAST_CAMPAIGN": "",
    },
]
df_example = {
    "DATE": "2020-03-31",
    "AGE": 50,
    "JOB_TYPE": "Technicien",
    "STATUS": "Marié",
    "EDUCATION": "Secondaire",
    "HAS_DEFAULT": "No",
    "BALANCE": 234,
    "HAS_HOUSING_LOAN": "No",
    "HAS_PERSO_LOAN": "No",
    "CONTACT": "Portable",
    "DURATION_CONTACT": 299,
    "NB_CONTACT": 1,
    "NB_DAY_LAST_CONTACT": -1,
    "NB_CONTACT_LAST_CAMPAIGN": 0,
    "RESULT_LAST_CAMPAIGN": "",
}
df_merged_res = {
    "DATE": "2020-03-31",
    "AGE": 50,
    "JOB_TYPE": "Technicien",
    "STATUS": "Marié",
    "EDUCATION": "Secondaire",
    "HAS_DEFAULT": "No",
    "BALANCE": 234,
    "HAS_HOUSING_LOAN": "No",
    "HAS_PERSO_LOAN": "No",
    "CONTACT": "Portable",
    "DURATION_CONTACT": 299,
    "NB_CONTACT": 1,
    "NB_DAY_LAST_CONTACT": -1,
    "NB_CONTACT_LAST_CAMPAIGN": 0,
    "RESULT_LAST_CAMPAIGN": "",
    "Pred_1": 1,
}


def write_csv_to_test(filename, datas):

    df_tmp = pd.DataFrame([data_test])
    df_tmp.to_csv(os.path.join(stg.DATA_DIR, filename), sep=stg.SEP)


def test_get_predict_from_json_working(test_app, mocker):
    def mock_load(self):
        return [[0, 1]]

    mocker.patch(
        "chaos.application.api.predict.Customer.predict_subscription_from_file",
        mock_load,
    )
    response = test_app.post("/predict", json=df_example)
    assert response.status_code == 200
    assert response.json() == {"prediction": {"0": 1}}


def test_get_predict_from_empty_json(test_app, mocker):
    def mock_load(self):
        return 1

    mocker.patch(
        "chaos.application.api.predict.Customer.predict_subscription_from_file",
        mock_load,
    )
    response = test_app.post("/predict", json={})
    assert response.status_code == 422
    # assert response.json() == {"prediction": 1}


def test_post_predict_from_empty_csv(test_app, mocker):
    _files = {"file": ""}

    def mock_load(self):
        return [0, 1]

    mocker.patch(
        "chaos.application.api.predict.Customer.predict_subscription_from_file",
        mock_load,
    )
    response = test_app.post("/multiples_predict", files=_files)
    assert response.status_code == 400
    assert response.json() == {"detail": "File is empty"}


def test_post_predict_from_csv(test_app, mocker):
    write_csv_to_test("test_file.csv", data_test)
    _test_upload_file = Path(os.path.join(stg.DATA_DIR, "test_file.csv"))
    _files = {"file": _test_upload_file.open("rb")}

    def mock_load(self):
        return [[0, 1]]

    mocker.patch(
        "chaos.application.api.predict.Customer.predict_subscription_from_file",
        mock_load,
    )

    response = test_app.post("/multiples_predict", files=_files)
    assert response.status_code == 200
    assert response.json() == {"prediction": {"0": 1}}


def test_create_dataframe_from_predictions():
    df_tmp = pd.DataFrame([df_example])
    predictions = [[0, 1]]
    df_merged = predict._create_dataframe_from_predictions(df_tmp, predictions)
    assert df_merged.shape == pd.DataFrame([df_merged_res]).shape
    assert df_merged["Pred_1"][0] == [1]


def test_get_predictions(test_app, mocker):
    write_csv_to_test("test_file.csv", df_example)
    file = os.path.join(stg.DATA_DIR, "test_file.csv")

    def mock_load(self):
        return [[0, 1]]

    mocker.patch(
        "chaos.application.api.predict.Customer.predict_subscription_from_file",
        mock_load,
    )
    test = predict._get_predictions(file)
    assert test["Pred_1"][0] == [1]
    assert test["Pred_0"][0] == [0]
