CREATE TABLE Course(..., PRIMARY KEY (CourseID),
				         FOREIGN KEY (OfferingID) REFERENCES Offering,
				   		 CONSTRAINT OfferingID NOT NULL)
						 
CREATE TABLE Student(..., PRIMARY KEY (StudentID))

CREATE TABLE Faculty(..., PRIMARY KEY(FacID), 
					 	  FOREIGN KEY (FacSupID) REFERENCES Faculty)
						  
CREATE TABLE Offering(..., PRIMARY KEY (OfferingID),
					 	   FOREIGN KEY (FacultyID) REFERENCES Faculty)
						   
CREATE TABLE Enrollment(..., PRIMARY KEY(OfferingID, StudentID),
					   		 FOREIGN KEY(OfferingID) REFERENCES Offering,
					         FOREIGN KEY(StudentID) REFERENCES Student,
					   		 CONSTRAINT OfferingID NOT NULL,
					   		 CONSTRAINT StudentID NOT NULL)	 
							 
							 
							 
