SELECT s.*
FROM (SELECT round(AVG(capacity_mw)) AS avg_capacity, country_long
	  FROM powerplant GROUP BY country_long) s
ORDER BY s.avg_capacity DESC
LIMIT 5;

SELECT AVG(estimated_generation_gwh) AS avg_estimated_ghw,
	CASE 
		WHEN capacity_mw < 100 THEN 'petite centrale'
		ELSE 'grosse centrale'
	END
	AS taille_centrale
FROM powerplant GROUP BY taille_centrale;


SELECT country_long AS country, COUNT(name) AS nb
FROM powerplant
WHERE capacity_mw > 200
GROUP BY country_long
HAVING COUNT(name) > 30
ORDER BY nb DESC;


SELECT COUNT(*)
FROM(
	 SELECT country_long AS country, COUNT("name") AS nb
	 FROM powerplant
	 WHERE capacity_mw > 200
	 GROUP BY country_long
	 HAVING COUNT("name") > 30
	 ORDER BY nb DESC
) powerplant;


SELECT COUNT(*) 
FROM artists
INNER JOIN artworks ON artists."ConstituentID" = artworks."ConstituentID"
WHERE artists."Nationality" = 'German';


SELECT artists."DisplayName" AS artist, artists."Nationality", artworks."Title", artworks."Date"
FROM artists
INNER JOIN artworks ON artists."ConstituentID" = artworks."ConstituentID"
WHERE artists."Gender" = 'Female'
ORDER BY artworks."Date"
LIMIT 10;



SELECT artists."DisplayName" AS artist, artists."Nationality" FROM artists
LEFT JOIN artworks
ON artists."ConstituentID" = artworks."ConstituentID"
WHERE (artworks."ConstituentID" IS NULL) AND (artists."Nationality" = 'French');


SELECT ARTISTS.country AS country, COUNT(ARTISTS.artworks_id) AS nb_oeuvres
FROM(SELECT artists."Nationality" AS country,
	 		artworks."ConstituentID" AS artworks_id
	 FROM artists
	 INNER JOIN artworks
	 ON artists."ConstituentID" = artworks."ConstituentID") ARTISTS 
GROUP BY ARTISTS.country
ORDER BY nb_oeuvres DESC







