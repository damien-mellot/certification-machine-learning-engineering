SELECT COUNT(*) 
FROM taxi_table
WHERE shared = 'N';

SELECT COUNT(*) 
FROM taxi_table
WHERE shared = 'Y' AND app = 'UBER';

SELECT COUNT(*)
FROM taxi_table
WHERE EXTRACT(MIN FROM dropoff_datetime - pickup_datetime) BETWEEN 20 AND 25;

SELECT COUNT(*)
FROM(
	SELECT DISTINCT dropoff_location, pickup_location
	FROM taxi_table
	WHERE EXTRACT(HOUR FROM dropoff_datetime - pickup_datetime) >= 1) taxi_table;
	
