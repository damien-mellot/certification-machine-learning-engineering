
SELECT DISTINCT country_long
FROM powerplant
WHERE capacity_mw > 60;

SELECT name, generation_gwh_2016, generation_gwh_2017,
	CASE 
		WHEN generation_gwh_2016 < generation_gwh_2017 THEN 'smaller'
		WHEN generation_gwh_2016 >= generation_gwh_2017 THEN 'bigger' 
		ELSE 'unknown' 
	END 
	AS growth_2017
FROM powerplant;

SELECT COUNT(*)
FROM(
	SELECT DISTINCT name
	FROM powerplant
	WHERE (commissioning_year <> 2017) OR (commissioning_year IS NULL)) powerplant;


