# forecast Project Repository

Copyright : 2021, Vinh Pham-Gia

Code example to solve forecast assignment

# Getting Started

### Setup instructions and run project
Project setup could be performed using the following command lines:
```
$ cd forecast-assignment/
$ poetry config virtualenvs.in-project true  # To create virtual environment in current project
$ poetry install --no-root
```

### Run script to access components
One must ensure that `FORECAST_SETTINGS_MODULE` environment variable in `forecast/settings/.env` 
is set to either `csv` or `parquet`, depending of the chosen file extension.

Python module could be computed with the following command line:
```
$ source activate.sh
$ python forecast/application/main.py --equipment both_devices --file_extension parquet
```

#### Equipment
Current project supports following equipments:
* `both_devices`
* `ordinateur`
* `telephone`

#### File extension
Current project supports following file extensions:
* `csv`
* `parquet`
