# You can import another set of settings:
from .base import *

# You should create your own configuration file (that imports base or dev)
# Make sure to define what settings you are using the in the file settings/.env

RAW_DATE_COL = 'date'
RAW_DATE_FORMAT = '%Y-%m-%d'
RAW_EQUIPMENT_COL = 'equipement'
RAW_CITY_COL = 'ville'
RAW_SALES_COL = 'CA'

LIST_CITIES_TO_KEEP = ['Bordeaux', 'Mont de Marsan']
