# You can import another set of settings:
from .base import *

# You should create your own configuration file (that imports base or dev)
# Make sure to define what settings you are using the in the file settings/.env

RAW_DATE_COL = 'Timestamp'
RAW_DATE_FORMAT = '%d%m%Y'
RAW_EQUIPMENT_COL = 'Equipment'
RAW_CITY_COL = 'Town'
RAW_SALES_COL = 'Sales'

LIST_CITIES_TO_KEEP = ['Bordeaux', 'Mont-de-Marsan']
