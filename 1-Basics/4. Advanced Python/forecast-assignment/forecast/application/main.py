"""Module to build dataset.

Example
-------
Script could be run with the following command line

    $ python forecast/application/main.py -e both_devices -fe parquet

Attributes
----------
PARSER: argparse.ArgumentParser

"""

from os.path import basename, join

import argparse
import logging
import os

from forecast.domain.sales_dataset import DatasetTransformer

import forecast.settings as stg

PARSER = argparse.ArgumentParser(description='Aggregate dataset information.')

PARSER.add_argument('--equipment', '-e', required=True, help='Equipment to filter', choices=['both_devices', 'ordinateur', 'telephone'])
PARSER.add_argument('--file_extension', '-fe', required=True, help='Extension of raw file to select', choices=['csv', 'parquet'])
ARGS = PARSER.parse_args()

SAVED_FILENAME = f'processed_data_{ARGS.equipment}_v{ARGS.file_extension}.csv'
stg.enable_logging(log_filename=f'{basename(__file__)}.log', logging_level=logging.DEBUG)

logging.info('-----------------------')
logging.info(f'Equipment filtered: {ARGS.equipment} | Extension of raw file: {ARGS.file_extension}')
logging.info('-----------------------')
logging.info('Load data ..')
filename = [file_ for file_ in os.listdir(stg.DATA_DIR) if file_.endswith(f'.{ARGS.file_extension}')][0]
DT = DatasetTransformer(filename=filename, equipment=ARGS.equipment)
logging.info('.. Done')

logging.info('Save data ..')
data = DT.build_dataset()
data.to_csv(join(stg.OUTPUTS_DIR, SAVED_FILENAME), index=False)
logging.info('.. Done')

logging.info(f'End of script {basename(__file__)}')
