"""This module provides classes to collect bank and school holidays.

Classes
-------
BankHolidays
SchoolHolidays

"""
from jours_feries_france import JoursFeries
from vacances_scolaires_france import SchoolHolidayDates

import pandas as pd

import forecast.settings as stg


class BankHolidays:
    """Get bank holidays from API.

    Properties
    ----------
    data: pandas.DataFrame

    """

    def __init__(self):
        """Class initialization."""
        pass

    @property
    def data(self):
        """Collect and aggregate data from API.

        Returns
        -------
        cleaned_bank_holidays: pandas.DataFrame

        """
        df_bank_holidays = pd.DataFrame()

        _YEAR_START = pd.to_datetime(stg.STR_START_DATE).year
        _YEAR_END = pd.to_datetime(stg.STR_END_DATE).year

        for int_year in range(_YEAR_START, _YEAR_END + 1):
            df_for_year = self._load_clean_bank_holiday(year=int_year)
            df_bank_holidays = pd.concat([df_bank_holidays, df_for_year], axis=0, ignore_index=True, sort=False)

        return df_bank_holidays

    def _load_clean_bank_holiday(self, year):
        dict_bank_holiday_year = JoursFeries.for_year(year=year)

        df_bank_holidays = pd.DataFrame.from_dict(dict_bank_holiday_year, orient='index')

        df_formatted = df_bank_holidays.reset_index()\
                                       .rename(columns={0: stg.BANK_HOLIDAYS_DATE_COL,
                                                        'index': stg.BANK_HOLIDAYS_NAME_COL})\
                                       .filter(items=[stg.BANK_HOLIDAYS_DATE_COL, stg.BANK_HOLIDAYS_NAME_COL])

        return df_formatted


class SchoolHolidays:
    """Get school holidays from API.

    Properties
    ----------
    data: pandas.DataFrame

    """

    def __init__(self):
        """Class initialization."""
        pass

    @property
    def data(self):
        """Collect and aggregate data from API.

        Returns
        -------
        df_school_holidays: pandas.DataFrame

        """
        df_school_holidays = pd.DataFrame()

        _YEAR_START = pd.to_datetime(stg.STR_START_DATE).year
        _YEAR_END = pd.to_datetime(stg.STR_END_DATE).year

        for int_year in range(_YEAR_START, _YEAR_END + 1):
            df_for_year = self._load_clean_school_holiday(year=int_year)
            df_school_holidays = pd.concat(
                [df_school_holidays, df_for_year], axis=0, ignore_index=True, sort=False)

        return df_school_holidays

    def _load_clean_school_holiday(self, year):
        dict_school_holiday_year = SchoolHolidayDates().holidays_for_year(year=year)

        df_school_holidays = pd.DataFrame.from_dict(dict_school_holiday_year, orient='index')

        df_renamed_cols = self._rename_columns(df=df_school_holidays)
        df_formatted = self._format_holiday_name_col(df=df_renamed_cols)

        return df_formatted

    def _rename_columns(self, df):
        DICT_COLUMNS_MAPPING = {
            stg.SCHOOL_HOLIDAYS_A_RAW_COL: stg.SCHOOL_HOLIDAYS_A_COL,
            stg.SCHOOL_HOLIDAYS_B_RAW_COL: stg.SCHOOL_HOLIDAYS_B_COL,
            stg.SCHOOL_HOLIDAYS_C_RAW_COL: stg.SCHOOL_HOLIDAYS_C_COL,
            stg.SCHOOL_HOLIDAYS_NAME_RAW_COL: stg.SCHOOL_HOLIDAYS_NAME_COL
        }

        return df.reset_index(drop=True).rename(columns=DICT_COLUMNS_MAPPING)

    def _format_holiday_name_col(self, df):
        df_formatted = df.assign(**{stg.SCHOOL_HOLIDAYS_NAME_COL: lambda df: df[stg.SCHOOL_HOLIDAYS_NAME_COL]
                                    .str.replace("'", " ")
                                    .str.extract(".*\s(.*)$", expand=False)
                                    .str.lower()
                                    })

        return df_formatted


if __name__ == '__main__':
    sh = BankHolidays()
    toto = sh.data
    sc = SchoolHolidays()
    toto = sc.data
