"""This module collect sales data from flat file.

Classes
-------
SalesData

"""
import pandas as pd

from os.path import join

import forecast.settings as stg


class SalesData:
    """Clean Sales data from flat file.

    Attributes
    ----------
    filename: string

    Properties
    ----------
    data: pandas.DataFrame
        The property data gathers all sales data.
    """

    def __init__(self, filename):
        """Class initialization."""
        self.filename = filename

    @property
    def data(self):
        """Collect sales data from file and clean them.

        Returns
        -------
        df_sales: pandas.DataFrame

        """
        if self.filename.endswith('.csv'):
            df = pd.read_csv(join(stg.DATA_DIR, self.filename))
        elif self.filename.endswith('.parquet'):
            df = pd.read_parquet(join(stg.DATA_DIR, self.filename))
        else:
            raise FileExistsError('Extension must be parquet of csv.')

        df_sales = self._clean_data(df=df)
        return df_sales

    def _clean_data(self, df):
        df_with_formatted_columns = self._format_columns(df=df)
        df_with_renamed_columns = self._rename_columns(df=df_with_formatted_columns)
        df_filtered = self._filter_cities(df=df_with_renamed_columns)
        df_cleaned = self._clean_equipment_column(df=df_filtered)

        df_sales = df_cleaned
        return df_sales

    def _format_columns(self, df):

        df_formatted = df.assign(**{stg.RAW_DATE_COL: lambda df: pd.to_datetime(df[stg.RAW_DATE_COL],
                                                                                format=stg.RAW_DATE_FORMAT)
                                                                   .apply(lambda x: x.strftime('%Y-%m-%d')),
                                    stg.RAW_SALES_COL: lambda df: df[stg.RAW_SALES_COL].apply(float)
                                    })
        return df_formatted

    def _rename_columns(self, df):
        DICT_COLUMNS_MAPPING = {
            stg.RAW_DATE_COL: stg.DATE_COL,
            stg.RAW_EQUIPMENT_COL: stg.EQUIPMENT_COL,
            stg.RAW_CITY_COL: stg.CITY_COL,
            stg.RAW_SALES_COL: stg.SALES_COL
        }
        return df.rename(columns=DICT_COLUMNS_MAPPING)

    def _filter_cities(self, df):
        df_filtered_cities = df.query(f'{stg.CITY_COL} in {stg.LIST_CITIES_TO_KEEP}')
        return df_filtered_cities

    def _clean_equipment_column(self, df):
        df_clean = df.assign(**{stg.EQUIPMENT_COL: lambda df: df[stg.EQUIPMENT_COL].str.lower().str.replace("é|ê", "e")}) \
                     .query(f'{stg.EQUIPMENT_COL}.isin({stg.LIST_PRODUCTS_TO_KEEP})')
        return df_clean


if __name__ == '__main__':
    sd = SalesData(filename='second_dataset.parquet')
    toto = sd.data
