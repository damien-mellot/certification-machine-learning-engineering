# -*- coding: utf-8 -*-
"""Module to create aggregated dataset.

Classes
-------
DatasetTransformer
SalesDataset

"""
import logging
import numpy as np
import pandas as pd

from calendar import monthrange as mr
from dateutil.relativedelta import relativedelta
from os.path import splitext, basename

import forecast.settings as stg

from forecast.infrastructure.sales import SalesData
from forecast.infrastructure.holidays import BankHolidays, SchoolHolidays


if __name__ == '__main__':
    stg.enable_logging(log_filename=f'{splitext(basename(__file__))[0]}.log', logging_level=logging.DEBUG)


class DatasetTransformer:
    """Collect target and raw features.

    Attributes
    ----------
    dataset: pandas.DataFrame

    Methods
    -------
    build_dataset

    """

    def __init__(self, **kwargs):
        """Initialize class."""
        self.dataset = self._gather_daily_sales_and_calendar_properties(**kwargs)

    def build_dataset(self):
        """Main method to aggregate and compute all features.

        Returns
        -------
        df_aggregated: pandas.DataFrame
        """
        logging.debug('Building sales features ..')
        with_sales_features = self._build_sales_features(df=self.dataset)
        logging.debug('.. Done')

        logging.debug('Building distance to closest bank holiday ..')
        with_distance_to_holiday = self._add_distance_to_closest_bank_holiday(df=with_sales_features)
        logging.debug('.. Done')

        df_aggregated = with_distance_to_holiday
        return df_aggregated

    def _build_sales_features(self, df):
        with_last_year_sales = self._add_last_year_sales(df=df)
        with_last_year_sales_same_weekday = self._add_last_year_sales_same_weekday(df=with_last_year_sales)

        return with_last_year_sales_same_weekday

    def _add_distance_to_closest_bank_holiday(self, df):
        all_dates = df[stg.DATE_COL].apply(pd.to_datetime)
        dates_holidays = BankHolidays().data.assign(**{stg.DATE_COL: lambda df: pd.to_datetime(df[stg.DATE_COL])})\
                                            .filter(items=[stg.DATE_COL]).squeeze().tolist()

        index_crossjoin = pd.MultiIndex.from_product([all_dates, dates_holidays],
                                                     names=[stg.DATE_COL, f'{stg.DATE_COL}_holidays'])
        df_crossjoin = (
            pd.DataFrame(index=index_crossjoin).reset_index()
            .assign(**{stg.DISTANCE_CLOSEST_BANK_HOLIDAY_COL:
                        lambda df: np.abs(df[stg.DATE_COL] - df[f'{stg.DATE_COL}_holidays']).dt.days})
            .assign(**{stg.DATE_COL: lambda df: df[stg.DATE_COL].apply(str)})
            .groupby(by=stg.DATE_COL)
            .agg({stg.DISTANCE_CLOSEST_BANK_HOLIDAY_COL: 'min'})
            .filter(items=[stg.DATE_COL, stg.DISTANCE_CLOSEST_BANK_HOLIDAY_COL])
            .reset_index(drop=True)
            .set_index(df[stg.DATE_COL])
        )

        with_distance = df.merge(right=df_crossjoin, on=stg.DATE_COL, how='left')
        return with_distance

    def _add_last_year_sales(self, df):
        df_last_year = self.dataset.filter(items=[stg.DATE_COL, stg.SALES_COL])\
                                   .assign(**{stg.DATE_COL: lambda df: pd.to_datetime(df[stg.DATE_COL])})\
                                   .assign(**{stg.DATE_COL: lambda df: df[stg.DATE_COL] + pd.DateOffset(years=1)})\
                                   .assign(**{stg.DATE_COL: lambda df: df[stg.DATE_COL].astype(str)})\
                                   .rename(columns={stg.SALES_COL: f'{stg.SALES_COL}_last_year'})\
                                   .drop_duplicates(stg.DATE_COL)

        with_last_year_sales = df.merge(right=df_last_year, on=stg.DATE_COL, how='left')
        return with_last_year_sales

    def _add_last_year_sales_same_weekday(self, df):
        df_last_year = self.dataset.filter(items=[stg.DATE_COL, stg.SALES_COL])\
                                   .assign(**{stg.DATE_COL: lambda df: pd.to_datetime(df[stg.DATE_COL])})\
                                   .assign(**{stg.DATE_COL: lambda df: df[stg.DATE_COL].apply(lambda x: x + relativedelta(years=1,
                                                                                                                          weekday=x.weekday()))})\
                                   .assign(**{stg.DATE_COL: lambda df: df[stg.DATE_COL].astype(str)}) \
                                   .rename(columns={stg.SALES_COL: f'{stg.SALES_COL}_last_year_same_weekday'})\
                                   .drop_duplicates(stg.DATE_COL)

        with_last_year_sales = df.merge(right=df_last_year, on=stg.DATE_COL, how='left')
        return with_last_year_sales

    def _gather_daily_sales_and_calendar_properties(self, **kwargs):
        df_dates = pd.DataFrame({stg.DATE_COL: pd.date_range(start=stg.STR_START_DATE, end=stg.STR_END_DATE).astype(str)})

        sd = SalesDataset(**kwargs)
        target, calendar_props = sd.daily_sales, sd.calendar_properties

        dataset = df_dates.merge(right=target, on=stg.DATE_COL, how='left')\
                          .merge(right=calendar_props, on=stg.DATE_COL, how='left')
        return dataset


class SalesDataset:
    """Gather all sales data.

    Attributes
    ----------
    data: pandas.DataFrame
        Dataframe including data

    Properties
    ----------
    daily_sales: pandas.DataFrame
    calendar_properties: pandas.DataFrame

    """

    def __init__(self, filename, equipment='both_devices'):
        """Initialize class.

        Parameters
        ----------
        filename: str
            CSV filename containing data
        equipment: str, default 'both_devices'
            * both_devices
            * ordinateur
            * telephone
        """
        try:
            self.data = self._load_sales_data_for_products(filename=filename,
                                                           equipment=equipment)
        except FileNotFoundError as error:
            raise FileNotFoundError(f'Error in SalesDataset initialization - {error}')

    @property
    def daily_sales(self):
        """Collect sales by day."""
        df_daily_sales = self.data.filter(items=[stg.DATE_COL, stg.SALES_COL])
        return df_daily_sales

    @property
    def calendar_properties(self):
        """Build features."""
        df_dates = pd.DataFrame({stg.DATE_COL: self.data[stg.DATE_COL]})

        df_with_weekday = self._add_weekday(df=df_dates)
        df_with_weekend = self._add_weekend(df=df_with_weekday)
        df_with_school_holidays = self._add_school_holidays(df=df_with_weekend)
        df_with_bank_holidays = self._add_bank_holidays(df=df_with_school_holidays)

        df_features = df_with_bank_holidays
        return df_features

    def _add_weekday(self, df):
        df_weekday = df.filter(items=[stg.DATE_COL])\
                       .assign(**{stg.WEEKDAY_COL: lambda df: df[stg.DATE_COL].apply(pd.to_datetime)
                                                                              .apply(lambda x: x.strftime('%A'))})

        df_with_weekday = df.merge(right=df_weekday, on=stg.DATE_COL, how='left')
        return df_with_weekday

    def _add_weekend(self, df):
        df_weekend = df.filter(items=[stg.DATE_COL])\
                       .assign(**{stg.IS_WEEKEND_COL: lambda df: df[stg.DATE_COL].apply(pd.to_datetime)
                                                                                 .apply(lambda x: x.weekday() >= 5)})

        df_with_weekend = df.merge(right=df_weekend, on=stg.DATE_COL, how='left')
        return df_with_weekend

    def _add_bank_holidays(self, df):
        df_bank_holidays = BankHolidays().data.assign(**{stg.DATE_COL: lambda df: df[stg.DATE_COL].astype(str),
                                                         stg.IS_BANK_HOLIDAY_COL: True})\
            .drop(columns=[stg.BANK_HOLIDAYS_NAME_COL])

        df_with_bank_holidays = df.merge(right=df_bank_holidays, on=stg.DATE_COL, how='left')\
                                  .fillna(value={stg.IS_BANK_HOLIDAY_COL: False})
        return df_with_bank_holidays

    def _add_school_holidays(self, df):
        df_school_holidays = SchoolHolidays().data.assign(**{stg.DATE_COL: lambda df: df[stg.DATE_COL].astype(str)})\
                                                  .filter(items=[stg.DATE_COL] + stg.LIST_SCHOOL_HOLIDAYS_COLS)

        df_with_school_holidays = df.merge(right=df_school_holidays, on=stg.DATE_COL, how='left')\
                                    .fillna(value={col: False for col in stg.LIST_SCHOOL_HOLIDAYS_COLS})
        return df_with_school_holidays

    def _load_sales_data_for_products(self, filename, equipment):
        df_sales = SalesData(filename=filename).data

        if equipment == 'both_devices':
            df = df_sales.groupby(stg.DATE_COL, as_index=False)\
                         .agg({stg.SALES_COL: 'sum'})
        else:
            df = df_sales.query(f'{stg.EQUIPMENT_COL} == "{equipment}"')\
                         .groupby(stg.DATE_COL, as_index=False)\
                         .agg({stg.SALES_COL: 'sum'})

        return df


if __name__ == '__main__':
    sd = SalesDataset(filename='initial_dataset.csv',
                      equipment='ordinateur')

    dt = DatasetTransformer(filename='initial_dataset.csv', equipment='both_devices')
    toto = dt.build_dataset()