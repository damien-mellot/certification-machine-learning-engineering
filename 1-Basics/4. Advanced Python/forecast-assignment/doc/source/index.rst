.. forecast documentation master file, created by
   sphinx-quickstart on Mon Apr  2 15:29:28 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to forecast's documentation!
=========================================================

Contents:

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   generated/forecast.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`