code package
============

Submodules
----------

.. toctree::
   :maxdepth: 1

   code.exercice1
   code.exercice2
   code.exercice3
   code.exercice4
   code.query

Module contents
---------------

.. automodule:: code
   :members:
   :undoc-members:
   :show-inheritance:
   :private-members:
