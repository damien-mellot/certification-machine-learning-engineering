# Create virtual env with venv and install pip
python3 -m venv .venv
pip install --upgrade pip

# Activate virtual env
. ./.venv/bin/activate

pip install -r requirements-dev.txt


export PYTHONPATH="$PYTHONPATH:$(pwd)"
