# content of conftest.py
import pytest
import smtplib

@pytest.fixture()
def smtp_connection():
    return smtplib.SMTP("smtp.gmail.com", 587, timeout=5)


