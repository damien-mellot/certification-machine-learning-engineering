
"""
Exercice : Create the two test case (lowercase user or raise environment error)
test_upper_to_lower and test_raise_exception
Tips : Use the monkeypatch.setenv method to change environment variables
"""
import pytest
from code.exercice4 import get_os_user_lower


def test_upper_to_lower(monkeypatch):
    monkeypatch.setenv('USER', 'PATRICK TIMSIT')
    assert get_os_user_lower() == 'patrick timsit'


def test_raise_exception(monkeypatch):
    monkeypatch.delenv('USER', raising=False)

    with pytest.raises(EnvironmentError, match="USER environment is not set."):
        get_os_user_lower()
