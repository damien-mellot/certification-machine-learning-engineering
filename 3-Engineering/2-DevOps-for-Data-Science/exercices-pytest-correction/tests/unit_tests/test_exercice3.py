import os
import shutil
import pytest
import time
from code.exercice3 import get_user
from code import query
"""
Part 1 : Create a fixture that :
1) create a new directory in data/ and create one file inside this directory
2) Yield the directory to the tests
3) Remove the directory

Check that the tests are passing

4) Bonus question : create a temp directory instead of a real directory

"""


@pytest.fixture(scope='module')
def directory():
    os.mkdir('data/tmp')
    f = open('data/tmp/testfile', "w")
    f.write('YOLO')
    yield 'data/tmp'
    f.close()
    shutil.rmtree('data/tmp')


def test_file_exist(directory):
    assert os.path.exists(directory)
    assert len(os.listdir(directory)) == 1


def test_only_one_file_in_directory(directory):
    assert os.path.exists(directory)
    assert len(os.listdir(directory)) == 1


def test_file_exist_2(tmpdir):
    d = tmpdir.mkdir("tmp")
    f = d.join("testfile")
    f.write("YOLO")
    assert os.path.exists(d)
    assert len(os.listdir(d)) == 1


def test_only_one_file_in_directory_2(tmpdir):
    d = tmpdir.mkdir("tmp")
    f = d.join("testfile")
    f.write('YOLO')
    assert os.path.exists(d)
    assert len(os.listdir(d)) == 1

# ------------------------------


"""
Part 2 : We need to test the behaviour of a function that query a specific user from a http api

1. Mock the query_and_decode_agent to fake the results and return a fixed dictionary instead
2. Mock the time.sleep method to not wait 10 second before launching the request
3. Implement a test that check a ValueError is launched when id < 0

"""


def test_get_user_under_20_return_NOT_OK(monkeypatch):
    def mockquery(x):
        return {'age': 17}
    monkeypatch.setattr(query,
                        "query_and_decode_agent", mockquery)

    def mocksleep(t):
        pass
    monkeypatch.setattr(time, "sleep", mocksleep)

    result = get_user(5)
    expected = "NOT OK"
    assert result == expected


def test_get_user_over_20_return_OK(monkeypatch):
    def mockquery(x):
        return {'age': 37}
    monkeypatch.setattr(query,
                        "query_and_decode_agent", mockquery)

    def mocksleep(t):
        pass
    monkeypatch.setattr(time, "sleep", mocksleep)

    result = get_user(5)
    expected = "OK"
    assert result == expected


def test_get_user_not_positive_id_raises_value_error():
    with pytest.raises(ValueError):
        get_user(-1)
