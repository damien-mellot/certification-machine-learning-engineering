import time
from urllib import error

from code import query


def get_user(id):
    """
    Query a random user
    If user age < 20 : return OK
    else return NOK
    :return:
    """
    # Wait 10 second before doing the request
    time.sleep(1)
    if id < 0:
        raise ValueError
    try:
        agent = query.query_and_decode_agent(
            "http://pplapi.com/batch/{}/sample.json".format(id))
    except error.HTTPError:
        agent = None
        time.sleep(10)
    if agent is not None:
        if agent['age'] > 20:
            return 'OK'
        else:
            return 'NOT OK'
    else:
        return "NOT OK"
