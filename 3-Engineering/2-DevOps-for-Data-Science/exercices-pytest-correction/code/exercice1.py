
"""
Test driven development : Write the function that pass the tests in tests/execice1.py
"""


def reverse_list(l):
    """
    Reverses order of elements in list l.
    """
    l.reverse()
    return l


# ------------------------------------------------------------------------------
# Implémenter le test avant
def reverse_string(s):
    """
    Reverses order of characters in string s.
    """
    return s[::-1]


# ------------------------------------------------------------------------------

def is_english_vowel(c):
    """
    Returns True if c is an english vowel
    and False otherwise.
    """
    return c.lower() in 'aeiouy'

# ------------------------------------------------------------------------------


def count_num_vowels(s):
    """
    Returns the number of vowels in a string s.
    """
    return sum([is_english_vowel(c) for c in s])

# ------------------------------------------------------------------------------


def histogram(l):
    """
    Converts a list of integers into a simple string histogram.
    """
    histo = ''
    for n in l[:-1]:
        histo += "#" * n + '\n'
    return histo + "#" * l[-1]

# ------------------------------------------------------------------------------
# Implémenter le test avant


def get_word_lengths(s):
    """
    Returns a list of integers representing
    the word lengths in string s.
    """
    return [len(w) for w in s.split(" ")]


# ------------------------------------------------------------------------------

def remove_substring(substring, string):
    """
    Returns string with all occurrences of substring removed.
    """
    return string.replace(substring, '')


# ------------------------------------------------------------------------------

def read_column(file_name, column_number, sep):
    """
    Reads column column_number from file file_name
    and returns the values as floats in a list.
    """
    return [float(x.split(sep)[column_number - 1]) for x in open(file_name).readlines()]
