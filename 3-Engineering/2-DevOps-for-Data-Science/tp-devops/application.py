import io
import os
import random

import boto3
import pandas as pd
from flask import Flask
from flask import Response, request

UPLOAD_FOLDER = 'data/'
ALLOWED_EXTENSIONS = {'csv'}
DYNAMO_TABLE = os.environ.get('STARTUP_SIGNUP_TABLE')

ddb = boto3.client('dynamodb', region_name='eu-west-3')
application = Flask(__name__)
application.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

def calculate_target(df):
    query = """
    ((AMOUNT > 1800)
    or ((AMOUNT_LAST_HOUR > 800) and (MERCHANT in ['Games']))
    or ((NB_TRANSACTIONS_LAST_HOUR > 8) and (MERCHANT in ['Withdrawal']))
    or ((NB_TRANSACTIONS_LAST_HOUR > 4) and (AMOUNT.div(AMOUNT_LAST_HOUR) > 6))
    or ((HOUR < 6) and (HOUR > 1) and (MERCHANT in ['Withdrawal', 'VirtualMoney']) and (AMOUNT > 1200)
    and (AMOUNT_LAST_HOUR > 2400)))
    and
    (
    (not (MERCHANT in ['Restaurant']))
    and (not (AMOUNT == 0))
    )
    """
    df['y'] = 0
    df.loc[df.query(query.replace('\n', '')).index, 'y'] = 1

    return df

def generate_data(target=False, size=10000):
    """
    New data
    :param target:
    :return:
    """

    columns = {'AMOUNT': list(range(0, 2000, 50)),
               'MERCHANT': ['Games', 'Withdrawal', 'VirtualMoney', 'Restaurant', 'Taxi'],
               'AMOUNT_LAST_HOUR': list(range(1, 3000, 50)),
               'NB_TRANSACTIONS_LAST_HOUR': list(range(10)),
               'HOUR': list(range(25))
               }

    df = pd.DataFrame()
    size = 20000
    for col in columns:
        df[col] = random.choices(columns[col], k=size)

    df = calculate_target(df)

    return df


def dataframe_to_csv_string(df):
    buffer = io.StringIO()
    df.to_csv(buffer, index=False)
    return buffer.getvalue()


@application.route('/')
def main_route():
    return "This is the main page for the devops evaluation, to get the train dataset use /get/train route," \
           " to get testing set use /get/predict and use POST request /post/predict to send your prediction"


@application.route("/get/train")
def get_data():
    """
    Create fake data and return it
    :return:
    """
    df = generate_data(target=True)
    csv = dataframe_to_csv_string(df)
    response = Response(
        csv,
        mimetype="text/csv",
        headers={"Content-disposition":
                     "attachment; filename=train.csv"})

    return response


@application.route("/get/predict")
def get_data_predict():
    """
    Create fake data and return it
    :return:
    """
    df = generate_data(target=False)
    csv = dataframe_to_csv_string(df)
    response = Response(
        csv,
        mimetype="text/csv",
        headers={"Content-disposition":
                     "attachment; filename=predict.csv"})

    return response


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


@application.route("/post/predict", methods=['POST', 'GET'])
def post_data():
    """
    Post data with name of group and a csv with predictions
    These predictions will be evaluated
    :return:
    """
    if request.method == 'POST':

        # check if the post request has the file part
        if 'file' not in request.files:
            return 'No file'
        file = request.files['file']
        name = request.values['name']
        print(name)

        # if user does not select file, browser also
        # submit an empty part without filename

        stream = io.StringIO(file.stream.read().decode("UTF-8"), newline=None)
        df = pd.read_csv(stream)
        df['y_pred'] = df['y']
        df = calculate_target(df)
        accuracy = (df['y'] == df['y_pred']).mean()
        if DYNAMO_TABLE is not None:
            ddb.put_item(
                TableName=DYNAMO_TABLE,
                Item={'name': {"S": name}, 'accuracy': {"S": str(float(accuracy))}})

        return '200 OK' + str(DYNAMO_TABLE)


if __name__ == "__main__":
    host = '0.0.0.0'
    application.run(host='0.0.0.0')