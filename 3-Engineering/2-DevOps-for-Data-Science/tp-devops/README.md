Install
-------

Install the virtual environment and the dependencies :
```bash
python3 -m "venv" virt
. virt/bin/activate
pip install -r requirements.txt
```

Deploy on AWS Beanstalk
-----------------------


Make an archive of the project that can be exported to aws elastic beanstalk : 
```
git archive -v -o ../myapp.zip --format=zip HEAD
```

the myapp.zip file can then be deployed on the elastic beanstalk console 
(overriding the eval-devops project)


 
Test
----

Launch the tests with pytest : 
```bash
pytest
```


The pipeline is triggered manually for each commit on master branch
You should use the gitlab GUI to deploy

- /train/get : With this GET request you can retrieve the training dataset for the problem
- /train/predict : Retrieve the testing dataset, on which you should do the prediction
- /post/predict : You must send a csv file containing all the full dataset with an added "y" column
containing your prediction