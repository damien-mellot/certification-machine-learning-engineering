# importing the requests library
import requests
from flask import url_for

import application
import tempfile
import os
import io
import pytest

URL_PROD = "http://evaldevops-env.2cqn4npi3p.eu-west-3.elasticbeanstalk.com"
#URL_PROD = 'http://0.0.0.0:5000'

@pytest.fixture
def client():
    db_fd, application.application.config['DATABASE'] = tempfile.mkstemp()
    application.application.config['TESTING'] = True

    with application.application.test_client() as client:
        yield client
    os.close(db_fd)
    os.unlink(application.application.config['DATABASE'])


def test_get_train(client):
    # sending get request and saving the response as response object
    response = client.get('/get/train')
    csv = response.data.decode('utf-8')
    with open('test_data/train.csv', 'w') as f:
        f.write(csv)

    assert response.status == '200 OK'


def test_get_predict(client):
    # api-endpoint
    # sending get request and saving the response as response object
    response = client.get('/get/predict')
    csv = response.data.decode('utf-8')
    with open('test_data/train.csv', 'w') as f:
        f.write(csv)

    assert response.status == '200 OK'


def test_upload_file(client):
    """Test can upload csv file"""
    data = {'name': 'this is a name', 'age': 12}
    data = {key: str(value) for key, value in data.items()}

    with open('test_data/train.csv', 'r') as f:
        data['file'] = (io.BytesIO(f.read().encode('utf-8')), 'result.csv')
        response = client.post(
            'post/predict', data=data
        )
        assert response.status == '200 OK'

def test_main_route(client):
    """Test can upload csv file"""
    # sending get request and saving the response as response object
    response = client.get('/')

    assert response.status == '200 OK'


def test_prod_is_woking():
    """
    Integration test that check that the post route is working with the production URL
    Will only pass if the app is deployed on AWS beanstalk
    """

    # api-endpoint
    with open('test_data/predict.csv', 'r') as f:
        url = URL_PROD + '/post/predict'
        # sending get request and saving the response as response object
        values = {'name': 'test_name'}
        response = requests.post(url, files={'file': io.BytesIO(f.read().encode('utf-8'))}, data=values)
    from pdb import set_trace; set_trace()
    assert response.status_code == 200