from crontab import CronTab

cron = CronTab(user='root')
job = cron.new(
    command='python3 /home/ec2-user/app/2020_tp_correction/correction/application/predict.py')
job.hour.every(1)
cron.write()


cron = CronTab(user='root')
job = cron.new(
    command="""python3 /home/ec2-user/app/2020_tp_correction/correction/application/train.py""")
job.day.every(1)
cron.write()
