#!/usr/bin/env python3
import logging
import schedule
import time
from correction import settings
from correction.infrastructure.files import FileManager
from correction.infrastructure.api_calls import get_train_data
from correction.domain.modelling import train_model
from correction.infrastructure.api_calls import get_predict_data, post_predict_data
from correction.domain.modelling import make_predictions

files = FileManager(settings.DATA_DIR)


def post_prediction():
    model = files.load('models/model.joblib')
    df = get_predict_data()
    df = make_predictions(model, df)
    status = post_predict_data(df)
    logging.info(f"HTTP RESPONSE = {status}")


def update_model():
    df = get_train_data()
    model = train_model(df)
    files.save(model, 'models/model.joblib')


if __name__ == "__main__":
    schedule.every().hour.do(post_prediction)
    schedule.every().day.at('19:00').do(train_model)
    while True:
        schedule.run_pending()
        time.sleep(1)
