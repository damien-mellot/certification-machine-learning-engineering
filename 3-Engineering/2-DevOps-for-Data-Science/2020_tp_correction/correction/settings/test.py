import logging

from .base import *


logger = logging.getLogger()
logger.setLevel(logging.WARNING)


API_ENDPOINT = "http://eval-devops.eu-west-3.elasticbeanstalk.com/"
GROUP_NAME = "LuisLeProf"

numeric_features = ['AMOUNT', 'AMOUNT_LAST_HOUR', 'NB_TRANSACTIONS_LAST_HOUR']
categorical_features = ['MERCHANT']
paramgrid = {'preprocessor__num__imputer__strategy': ['median'],
             'classifier__max_depth': [1, 2], 'classifier__n_estimators': [3, 4]}
cv = 2
