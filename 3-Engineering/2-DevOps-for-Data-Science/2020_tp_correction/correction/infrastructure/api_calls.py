import io
import requests
import pandas as pd
from correction import settings


def get_train_data():
    return pd.read_csv(settings.API_ENDPOINT + '/get/train')


def get_predict_data():
    return pd.read_csv(settings.API_ENDPOINT + '/get/predict')


def post_predict_data(df):
    url = settings.API_ENDPOINT + '/post/predict'
    values = {"name": settings.GROUP_NAME}
    file = io.BytesIO(df.to_csv().encode())
    r = requests.post(url, files={'file': file}, data=values)
    return r.status_code
