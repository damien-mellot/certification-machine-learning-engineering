import pytest
import requests
import pandas as pd
import numpy as np
from sklearn.dummy import DummyClassifier


@pytest.fixture()
def sample_df():
    df = pd.DataFrame([[500, 'Games', 340, 1, 15, 0],
                       [1000, 'VirtualMoney', 567, 5, 11, 1],
                       [np.nan, 'Market', 577, 2, 13, 0],
                       [10000, np.nan, 577, 2, 13, 0],
                       [300, "Games", 801, 1, 7, 1],
                       [750, "Withdrawal", np.nan, 9, 12, 1],
                       [1500, "VirtualMoney", 751, 6, 12, 0],
                       [200, "Withdrawal", 2001, 1, 23, 0],
                       [1800, "VirtualMoney", 651, 9, 21, 0]],

                      columns=["AMOUNT", "MERCHANT", "AMOUNT_LAST_HOUR",
                               "NB_TRANSACTIONS_LAST_HOUR", "HOUR", "y"]
                      )
    return df


@pytest.fixture()
def sample_X(sample_df):
    return sample_df.drop('y', axis=1)


@pytest.fixture()
def sample_X_processed():
    X = np.array([[-0.47393037, -0.93454057, -0.9486833, 1., 0.,
                   0., 0., 0.],
                  [-0.30228557, -0.43460174, 0.31622777, 0., 0.,
                   1., 0., 0.],
                  [-0.34519677, -0.412578, -0.63245553, 0., 1.,
                   0., 0., 0.],
                  [2.78732085, -0.412578, -0.63245553, 0., 0.,
                   0., 0., 1.],
                  [-0.54258829, 0.0807537, -0.9486833, 1., 0.,
                   0., 0., 0.],
                  [-0.38810797, -0.33109018, 1.58113883, 0., 0.,
                   0., 1., 0.],
                  [-0.13064077, -0.02936498, 0.63245553, 0., 0.,
                   1., 0., 0.],
                  [-0.57691725, 2.72360212, -0.9486833, 0., 0.,
                   0., 1., 0.],
                  [-0.02765388, -0.24960235, 1.58113883, 0., 0.,
                   1., 0., 0.]])
    return X


@ pytest.fixture()
def sample_y(sample_df):
    y = sample_df['y']
    return y

# monkeypatched requests.get moved to a fixture


@pytest.fixture
def mock_response(monkeypatch):
    """Requests.get() mocked to return {'mock_key':'mock_response'}."""

    def mock_post(*args, **kwargs):
        r = requests.Response()
        r.status_code = 200
        return r

    monkeypatch.setattr(requests, "post", mock_post)


@pytest.fixture
def classifier(sample_X_processed, sample_y):

    return DummyClassifier(strategy="stratified").fit(sample_X_processed, sample_y)
