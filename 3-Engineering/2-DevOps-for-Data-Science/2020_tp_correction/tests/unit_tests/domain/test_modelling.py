import pandas as pd
from correction.settings import test as settings
from sklearn.compose import ColumnTransformer
from sklearn.pipeline import Pipeline
from sklearn.model_selection import GridSearchCV
from sklearn.utils.validation import check_is_fitted
from correction.domain.modelling import make_pipeline, make_preprocessor, make_predictions, gridsearch, train_model


def test_make_preprocessor():
    numeric = settings.numeric_features
    cat = settings.numeric_features
    preprocessor = make_preprocessor(numeric, cat)
    assert isinstance(preprocessor, ColumnTransformer)


def test_make_pipeline():
    numeric = settings.numeric_features
    cat = settings.numeric_features
    pipeline = make_pipeline(numeric, cat)

    assert isinstance(pipeline, Pipeline)
    assert len(pipeline.steps) > 0  # no empty pipeline


def test_gridsearch(sample_X, sample_y):
    clf = make_pipeline(settings.numeric_features, settings.categorical_features)
    estimator = gridsearch(sample_X, sample_y, clf, settings.paramgrid, settings.cv)
    assert isinstance(estimator, GridSearchCV)


def test_train_model(sample_df):
    # mock gridsearch
    fitted_clf = train_model(sample_df)

    for step in fitted_clf.steps:
        assert check_is_fitted(fitted_clf[1]) is None
    assert isinstance(fitted_clf, Pipeline)


def test_make_predictions(classifier, sample_df):
    df = make_predictions(classifier, sample_df)

    pd.testing.assert_frame_equal(df.drop('y', axis=1), sample_df.drop('y', axis=1))
    assert set(df['y']).issubset({0, 1})
