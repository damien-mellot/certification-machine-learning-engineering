import pytest
import pandas as pd
import requests
from correction.infrastructure.api_calls import get_predict_data, get_train_data, post_predict_data


def test_get_train_data():
    df = get_train_data()
    assert len(df) > 0


def test_get_predict_data():
    df = get_predict_data()
    assert len(df) > 0


def test_post_predict_data(monkeypatch, mock_response, sample_df):
    response = post_predict_data(sample_df)
    assert response == requests.codes.ok
