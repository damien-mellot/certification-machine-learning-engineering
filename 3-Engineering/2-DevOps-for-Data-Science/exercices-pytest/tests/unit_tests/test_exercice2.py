import pytest
from code.exercice2 import example_add_a_and_b, sum_of_multiple_of_3_and_5, even_fibonacci_numbers
"""
Write parametrized tests here:
"""


@pytest.mark.parametrize('a,b,expected', [(1, 1, 2), (2, 3, 5)])
def test_example_add_a_and_b(a, b, expected):
    assert example_add_a_and_b(a, b) == expected  # Basic assertion


def test_sum_of_multiple_of_3_and_5():
    pass


def test_even_fibonacci_numbers():
    pass
