from urllib import request
import json


def query_and_decode_agent(url):
    """
    Do a query with the provided url, return the first result
    :param url:
    :return:
    """
    response = request.urlopen(url)
    agent = json.loads(response.read().decode("utf8"))[0]
    return agent
