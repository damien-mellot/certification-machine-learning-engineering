
"""
Test driven development : Write the function that pass the tests in tests/execice1.py
"""


def reverse_list(l):
    """
    Reverses order of elements in list l.
    """
    # TODO
    return None


# ------------------------------------------------------------------------------
# Implémenter le test avant
def reverse_string(s):
    """
    Reverses order of characters in string s.
    """
    return None


# ------------------------------------------------------------------------------

def is_english_vowel(c):
    """
    Returns True if c is an english vowel
    and False otherwise.
    """
    return None

# ------------------------------------------------------------------------------


def count_num_vowels(s):
    """
    Returns the number of vowels in a string s.
    """
    return None

# ------------------------------------------------------------------------------


def histogram(l):
    """
    Converts a list of integers into a simple string histogram.
    """
    return None

# ------------------------------------------------------------------------------
# Implémenter le test avant


def get_word_lengths(s):
    """
    Returns a list of integers representing
    the word lengths in string s.
    """
    # TODO
    return None


# ------------------------------------------------------------------------------

def remove_substring(substring, string):
    """
    Returns string with all occurrences of substring removed.
    """
    # TODO
    return None


# ------------------------------------------------------------------------------
def read_column(file_name, column_number, sep):
    """
    Reads column column_number from file file_name
    and returns the values as floats in a list.
    """
    # TODO

    return None
