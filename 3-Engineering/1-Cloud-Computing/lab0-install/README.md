## Install GCP CLI:

https://cloud.google.com/sdk/docs/downloads-interactive

If necessary Execute:

    gcloud auth login
    gcloud config set project
    
Authenticate to a Google Cloud API:

[Creating a service account](https://cloud.google.com/docs/authentication/getting-started#creating_a_service_account)

[Setting the environment variable](https://cloud.google.com/docs/authentication/getting-started#setting_the_environment_variable)

[Verifying authentication](https://cloud.google.com/docs/authentication/getting-started#verifying_authentication)


## Install the AWS CLI:

**Creating, Disabling, and Deleting Access Keys for Your AWS Account Root User**

https://docs.aws.amazon.com/general/latest/gr/managing-aws-access-keys.html

Install AWS CLI

https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2.html

    aws configure



