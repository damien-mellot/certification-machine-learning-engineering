# Elastic Beanstalk

Run the following command to install the Elastic Beanstalk CLI:

    pip install awsebcli --upgrade --user

### Run Hello World on your local machine

Navigate to your project directory and install dependencies:

    cd hello-world-flask

To run the Hello World app on your local computer create a virtual environment and install dependencies:

    Create an isolated Python environment in a directory external to your project and activate it:

    python3 -m venv env
    source env/bin/activate
    pip install  -r requirements.txt

Run the application:

    python application.py

In your web browser, enter the following address:

http://localhost:8080

### Deploy and run Hello World on Elastic Beanstalk

Initialize your EB CLI repository with the eb init command:

    eb init -p python-3.6 hello-world-flask --region eu-west-1
    
Create a new environment for your application

    eb create flask-env
    
Open your web site with eb open
    
    eb open
    
To terminate your Elastic Beanstalk environment
        
    eb terminate flask-env

    
    
    
    
    
    
    
