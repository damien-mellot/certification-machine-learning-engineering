# DWH - Lab

## Your challenge

Your challenge is to create a load the kddcup.data_10_percent.gz dataset into a cloud storage bucket 
and analyze the data with a pyspark job launched from a notebook (spark_on_cloud_hadoop.ipynb).

# Create a cloud storage bucket and upload the data

Create a cloud storage bucket and upload the file kddcup.data_10_percent.gz

# Create a hadoop cluster

From AWS EMR Console / Clusters click on Create cluster

![](images/step1.png)

Click on Go to advanced options

![](images/step2.png)

Make sure you select at least these software : Hadoop, Hive, Spark and Livy. Click on Next

![](images/step3.png)

Choose your default vpc as the network of the cluster. Click on Next

![](images/step4.png)

Name your cluster and uncheck Logging. Click on Next

![](images/step5.png)

Click on Create cluster

![](images/step6.png)

Your cluster should be starting. Wait until it is in a "Waiting" state.

![](images/step7.png)

# Upload the spark_on_cloud_hadoop.ipynb and launch pyspark jobs to analyze the data

Once your cluster is in "Waiting" state. From EMR Console / Notebooks, click on Create notebook

![](images/step8.png)

Name your notebook and choose your cluster, click on create Notebook.

![](images/step9.png)

Once your notebook is in "Ready" state, click on Open in JupyterLab

![](images/step10.png)

Drag and drop spark_on_cloud.ipynb and make sure the kernel used is PySpark.
Replace the BUCKET variable with the name of your bucket
Replace the STORAGE variable with "s3"

![](images/step11.png)

Execute all the cells and verify that the results of your analysis are in your s3 bucket

## Clean up

Don't forget to delete your notebook, your cluster and your cloud storage bucket!