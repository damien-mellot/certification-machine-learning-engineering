# DWH - Lab

## Your challenge

Your challenge is to create a load the nyc_tlc_yellow_trips_2018_subset_1.csv dataset into a cloud datawarehouse and analyze the data.

# Creat a cloud storage bucket and upload the data

## Create a new dataset to store tables

In the Redshift console, nevigate to config, create a cluster subnet group

![](images/step1.png)

Choose your VPC and click on "Add all the subnets for this VPC", click on "Create cluster subnet group"

![](images/step2.png)

In the home screen of the Redshift console, click on "Create cluster"

![](images/step3.png)

Choose a dc2.large cluster with 1 node

![](images/step4.png)

Name your cluster yotta and choose a password for user awsuser.

![](images/step5.png)

Name the database nyctaxi

![](images/step6.png)

Create your cluster

![](images/step7.png)

## Ingest a new Dataset from a CSV

Create a S3 bucket (ex labdwh), upload nyc_tlc_yellow_trips_2018_subset_1.csv and nyc_tlc_yellow_trips_2018_subset_2.csv

In Redshift/Editor console connect to your database 

![](images/step8.png)


In the Editor, Create a table with the request :

    CREATE TABLE trips 
    (
      vendor_id	INTEGER,
      pickup_datetime	TIMESTAMP,
      dropoff_datetime	TIMESTAMP,
      passenger_count	INTEGER,
      trip_distance	FLOAT,
      rate_code	INTEGER,
      store_and_fwd_flag	BOOLEAN,
      payment_type	INTEGER,
      fare_amount	FLOAT,
      extra	FLOAT,
      mta_tax	FLOAT,
      tip_amount	FLOAT,
      tolls_amount	FLOAT,
      imp_surcharge	FLOAT,
      total_amount	FLOAT,
      pickup_location_id	INTEGER,
      dropoff_location_id	INTEGER
    );
    
Load the data from S3 with the request :

    COPY trips
    FROM 's3://<BUCKET_NAME>/nyc_tlc_yellow_trips_2018_subset_1.csv'
    credentials 'aws_access_key_id=<Your-Access-Key-ID>;aws_secret_access_key=<Your-Secret-Access-Key>'
    CSV
    IGNOREHEADER 1
    TIMEFORMAT AS 'YYYY-MM-DDTHH:MI:SS';

## Analyze the data

practice with a basic query on the trips table

In the Query Editor, write a query to list the top 5 most expensive trips of the year

    SELECT
      *
    FROM
      trips
    ORDER BY
      fare_amount DESC
    LIMIT  5

## Ingest a new Dataset from Cloud Storage

Load the data from S3 with the request :

    COPY trips
    FROM 's3://<BUCKET_NAME>/nyc_tlc_yellow_trips_2018_subset_2.csv'
    credentials 'aws_access_key_id=<Your-Access-Key-ID>;aws_secret_access_key=<Your-Secret-Access-Key>'
    CSV
    IGNOREHEADER 1
    TIMEFORMAT AS 'YYYY-MM-DDTHH:MI:SS';
    
Confirm that the row count has now almost doubled.

You may want to run the same query like earlier to see if the top 5 most expensive trips have changed.

## Create tables from other tables with DDL

The trips table now has trips from throughout the year. What if you were only interested in January trips? For the purpose of this lab, we will keep it simple and focus only on pickup date and time. Let's use DDL to extract this data and store it in another table

In the Query Editor, run the following CREATE TABLE command

    CREATE TABLE
      january_trips AS
    SELECT
      *
    FROM
      trips
    WHERE
      EXTRACT(Month
      FROM
        pickup_datetime)=1;
        
Now run the below query in your Query Editor find the longest distance traveled in the month of January

    SELECT
      *
    FROM
      january_trips
    ORDER BY
      trip_distance DESC
    LIMIT
      1

## Clean up

Don't forget to delete your dataset and cloud storage bucket!
