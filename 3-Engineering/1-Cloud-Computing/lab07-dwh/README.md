# DB - Lab

## Your challenge

Your challenge is to create and load the nyc_tlc_yellow_trips_2018_subset_1.csv dataset into a cloud datawarehouse to analyze the data.

# Creat a cloud storage bucket and upload the data

nyc_tlc_yellow_trips_2018_subset_1.csv
and 
nyc_tlc_yellow_trips_2018_subset_2.csv

## Use the cloud data warehouse to create a table and load the data

Create a database/dataset nyctaxi

Load the nyc_tlc_yellow_trips_2018_subset_1.csv into a table trips

## Analyze the data

What are the top 5 most expensive trips of the year?

## Ingest a new Dataset from Cloud Storage

NLoad the nyc_tlc_yellow_trips_2018_subset_2.csv from cloud storage.

## Create tables from other tables with DDL

The 2018trips table now has trips from throughout the year. 
What if you were only interested in January trips? 
For the purpose of this lab, we will keep it simple and focus only on pickup date and time. 
Let's use DDL to extract this data and store it in another table

Find the longest distance traveled in the month of January

## Clean up

Don't forget to delete your dataset and cloud storage bucket!
