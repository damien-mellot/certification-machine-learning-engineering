## Load Balancing and Autoscaling

###Create the health check firewall rules
Health checks determine which instances of a load balancer can receive new connections. For HTTP load balancing, the health check probes to your load-balanced instances come from addresses in the ranges 130.211.0.0/22 and 35.191.0.0/16. Your firewall rules must allow these connections.

![](images/step1.png)

Specify the following, and leave the remaining settings as their defaults:


![](images/step2.png)

![](images/step3.png)

Click Create.

### Create a custom web server image for the backend of the load balancer.

Create a VM
In the GCP Console, on the Navigation menu (Navigation menu), click Compute Engine > VM instances.

Click Create Instance.

![](images/step4.png)

Specify the following, and leave the remaining settings as their defaults:

![](images/step5.png)

Click Disks, and clear Delete boot disk when instance is deleted.

![](images/step6.png)

![](images/step7.png)

Click Create.

### Customize the VM
For instance-1, click SSH to launch a terminal and connect.

![](images/step8.png)

To install Apache2, run the following commands:

    sudo apt-get update
    sudo apt-get install -y apache2


To start the Apache server, run the following command:

    sudo service apache2 start

In the GCP Console, for instance-1, click the External IP address.

![](images/step9.png)

The default page for the Apache2 server should be displayed.

Set the Apache service to start at boot
The software installation was successful. However, when a new VM is created using this image, the freshly booted VM does not have the Apache web server running. Use the following command to set the Apache service to automatically start on boot. Then test it to make sure it works.

In the instance-1 SSH terminal, set the service to start on boot:

    sudo update-rc.d apache2 enable

In the GCP Console, select instance-1, and then click Reset.
In the confirmation dialog, click Reset.
Reset will stop and reboot the machine. It keeps the same IPs and the same persistent boot disk, but memory is wiped. Therefore, if the Apache service is available after the reset, the update-rc command was successful.

For instance-1, click the External IP address of the instance to verify that the Apache service is available. You should see the default page.

You can also check the server by connecting via SSH to the VM and entering the following command:

    sudo service apache2 status

The result should show Started The Apache HTTP Server.

Return to the VM instances page, click instance-1, and click Delete.
In the confirmation dialog, click Delete.

![](images/step11.png)

In the left pane, click Disks and verify that the instance-1 disk exists.

![](images/step12.png)

### Create the custom image
In the left pane, click Images.
Click Create image.

![](images/step13.png)

Specify the following, and leave the remaining settings as their defaults:

![](images/step14.png)

You have created a custom image that multiple identical webservers can be started from. At this point, you could delete the instance-1 disk.

The next step is to use that image to define an instance template that can be used in the managed instance groups.

### Create a custom image for a web server

Configure an instance template and create instance groups
A managed instance group uses an instance template to create a group of identical instances. Use these to create the backends of the HTTP load balancer.

### Configure the instance template
An instance template is an API resource that you can use to create VM instances and managed instance groups. Instance templates define the machine type, boot disk image, subnet, labels, and other instance properties.

In the GCP Console, on the Navigation menu (Navigation menu), click Compute Engine > Instance templates.

![](images/step15.png)

Click Create instance template.

For Name, type instance-template-1.

![](images/step16.png)

For Machine type, select f1-micro (1 vCPU).

For Boot disk, click Change.

Click Custom images.

Select image-1.

![](images/step17.png)

Click Select.

Click Management, security, disks, networking, sole tenancy.

Click Networking.

Specify the following, and leave the remaining settings as their defaults:

![](images/step18.png)

Click Create.

### Create the managed instance groups

Create a managed instance group in us-central1 and one in europe-west1.

On the Navigation menu, click Compute Engine > Instance groups.

![](images/step19.png)

Click Create Instance group.

Specify the following, and leave the remaining settings as their defaults:

![](images/step20.png)
![](images/step21.png)

For Health check, select Create a health check.

Specify the following, and leave the remaining settings as their defaults:

![](images/step22.png)

Click Save and continue.

For Initial delay, type 60. This is how long the Instance Group waits after initializing the boot-up of a VM before it tries a health check. You don't want to wait 5 minutes for this during the lab, so you set it to 1 minute.

![](images/step23.png)

Click Create.

![](images/step24.png)

NOTE: If a warning window will appear stating that There is no backend service attached to the instance group. Ignore this; you will configure the load balancer with a backend service in the next section of the lab.
Click OK.

Repeat the same procedure for instancegroup-2 in us-central1.

Verify the backends
Verify that VM instances are being created in both regions, and access their HTTP sites.

On the Navigation menu, click Compute Engine > VM instances. Notice the instances that start with instancegroup-2 and instancegroup-1. 
These instances are part of the managed instance groups.

Click on the External IP of an instance.

The default page for the Apache2 server should be displayed.

### Configure the HTTP load balancer

Configure the HTTP load balancer to balance traffic between the two backends (us-central1-mig in us-central1 and europe-west1-mig in europe-west1) as illustrated in the network diagram:

network_diagram.png

Start the configuration
On the Navigation menu, click Network Services > Load balancing.

![](images/step25.png)

Click Create load balancer.

![](images/step26.png)

Under HTTP(S) Load Balancing, click Start configuration.

![](images/step27.png)

Select From Internet to my VMs, then click Continue.

![](images/step28.png)

For Name, type http-lb.

![](images/step29.png)

Configure the backend
Backend services direct incoming traffic to one or more attached backends. Each backend is composed of an instance group and additional serving capacity metadata.

Click Backend configuration.

For Backend services & backend buckets, click Create or select backend services & backend buckets > Backend services > Create a backend service.

![](images/step30.png)

Specify the following, and leave the remaining settings as their defaults:

![](images/step31.png)

This configuration means that the load balancer attempts to keep each instance of instance-group-1 at or below 50 requests per second (RPS).

Click Done.

Click Add backend. Add instance-group-2.

![](images/step32.png)

Click Done.

For Health Check, select http-health-check (TCP).

Click Create.

Configure the frontend
The host and path rules determine how your traffic will be directed. For example, you could direct video traffic to one backend and direct static traffic to another backend. However, you are not configuring the host and path rules in this lab.

Click Frontend configuration.

Specify the following, and leave the remaining settings as their defaults:

![](images/step33.png)

Click Done.

HTTP(S) load balancing supports both IPv4 and IPv6 addresses for client traffic. 
Client IPv6 requests are terminated at the global load balancing layer and then proxied over IPv4 to your backends.

Review and create the HTTP load balancer
Click Review and finalize.

![](images/step34.png)

Review the Backend services and Frontend.
Click Create. Wait for the load balancer to be created.
Click on the name of the load balancer (http-lb).
Note the IPv4  addresses of the load balancer for the next task.

Now that you have created the HTTP load balancer for your backends, it is time to verify that traffic is forwarded to the backend service.

The HTTP load balancer should forward traffic to the region that is closest to you. 

### Access the HTTP load balancer

Open a new tab in your browser and navigate to http://[LB_IP]. 
Make sure to replace [LB_IP] with the IPv4 address of the load balancer.
Accessing the HTTP load balancer might take a couple of minutes. 
In the meantime, you might get a 404 or 502 error. Keep trying until you see the page of one of the backends.

### Stress test the HTTP load balancer

Create a new VM to simulate a load on the HTTP load balancer. 
Then determine whether traffic is balanced across both backends when the load is high.

In the GCP Console, on the Navigation menu (Navigation menu), click Compute Engine > VM instances.

Click Create instance.

Specify the following, and leave the remaining settings as their defaults:

Property	Value (type value or select option as specified)
Name	stress-test
Region	us-west1
Zone	us-west1-c
Machine type	f1-micro (1 vCPU)
Because us-west1 is closer to us-central1 than to europe-west1, traffic should be forwarded only to us-central1-mig (unless the load is too high).

For Boot Disk, click Change.

Click Custom images.

Select image-1.

Click Select.

Click Create. Wait for the stress-test instance to be created.

For stress-test, click SSH to launch a terminal and connect.

To create an environment variable for your load balancer IP address, run the following command:

    export LB_IP=<Enter [YOUR_LB_IP] here>

Verify it with echo:

    echo $LB_IP

To place a load on the load balancer, run the following command:

    ab -n 500000 -c 1000 http://$LB_IP/

Stress test the HTTP load balancer

Depending on the load, you might see the backends scale to accommodate the load.