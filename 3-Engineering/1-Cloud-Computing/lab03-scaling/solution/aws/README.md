## AWS


### Create an image from an ec2 instance

Create an EC2 instance:

Instance type   t2.micro
Security Group allowing
    ssh (tcp) port 22
    tcp port 80
AMI ubuntu-18.04
An existing or new key pair.

Connect to your instance with ssh. 

To install Apache2, run the following commands:

    sudo apt-get update
    sudo apt-get install -y apache2


To start the Apache server, run the following command:

    sudo service apache2 start

In the Console, for webserver, click the External IP address.

The default page for the Apache2 server should be displayed.

![](images/step0.png)

Set the Apache service to start at boot
The software installation was successful. However, when a new VM is created using this image, the freshly booted VM does not have the Apache web server running. Use the following command to set the Apache service to automatically start on boot. Then test it to make sure it works.

In the webserver SSH terminal, set the service to start on boot:

    sudo update-rc.d apache2 enable



Create your AMI.
Select the newly created instance and launch Action > Image > Create Image


![](images/step5.png)

Name your image image-1.

![](images/step6.png)

Verify that your image is pending.

![](images/step7.png)

### Create an Application Load Balancer

Create an Application Load Balancer to load balance between EC2 instances you will create later on inside your Auto Scaling group:

Navigate to the EC2 portion of the console.
Click on the Load Balancers section under Load Balancing.

![](images/step1.png)

Press the Create button under the Application Load Balancer.

![](images/step2.png)

Name your load balancer http-lb, leave the ALB set to internet facing, and set the IP address type as ipv4.
Select the VPC, and add all the AZ for eu-west.

![](images/step3.png)

Configure a target group for your ALB, naming it http-group.
Expand Advanced health check settings, and reduce the healthy threshold check down to 2.
Proceed to create your ALB.

![](images/step4.png)

### Create a launch template that will be used by the Auto Scaling group:

Navigate to EC2 > Instances > Launch Templates.

![](images/step8.png)

Create a new template, and call it template-1.
The Source Template is none.
Use the image-1 AMI.

![](images/step9.png)

Set the instance type as t3.micro (not T3a.micro).
Select the key pair created earlier.
Network type is VPC.
Select the security group used/created earlier.


![](images/step10.png)

Storage should automatically be populated with a volume, so leave that as default and don't add anything to the network section.
Click Create Launch Template.

![](images/step11.png)

### Create an Auto Scaling Group

Click Create Auto Scaling group.

![](images/step12.png)

Select Launch Template, and choose the template you just created. 
NOTE: If you receive the message No default VPC found, you can ignore it as we are selecting our VPC below.
Pick the VPC from the lab environment.

![](images/step13.png)

Click Next: Configure Scaling Policies.
Scale between 2 and 5 instances.
Add a scaling policy :
Metric type : ALB Request Count Per Target : 50 for your target group.

![](images/step14.png)

Click Next: Configure Notifications > Next: Configure Tags > Review > Create Auto Scaling group > Close.

![](images/step15.png)

Note: Make sure the load balancer is ready at this point.


### Stress test the auto scaling group

Note the DNS name of your load balancer

![](images/step16.png)

Connect to your EC2 instance with SSH.

To create an environment variable for your load balancer IP address, run the following command:

    export LB_DNS=<Enter [LB_DNS] here>

Verify it with echo:

    echo LB_DNS

To place a load on the load balancer, run the following command:

    ab -n 500000 -c 1000 http://LB_DNS/

Stress test the HTTP load balancer

Depending on the load, you might see the backends scale to accommodate the load.