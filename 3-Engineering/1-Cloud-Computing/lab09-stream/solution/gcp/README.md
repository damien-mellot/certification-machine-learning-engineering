## Objectives

In this lab, you:

Use SQL to join Pub/Sub streaming data with BigQuery table data
Deploy a Dataflow job from the Dataflow SQL UI

## Sources

We will create the following sources:

* A Pub/Sub topic called transactions - A stream of transaction data that arrives via a subscription to the Pub/Sub topic. The data for each transaction includes information like the product purchased, the sale price, and the city and state in which the purchase occurred. After you create the Pub/Sub topic, you run a script that publishes messages to your topic. 

* A BigQuery table called us_state_salesregions - A table that provides a mapping of states to sales regions. Before you create this table, you need to create a BigQuery dataset.

## Create a Pub/Sub topic and publisher script


From Pub/Sub Console click Create Topic

![](images/step1.png)

Or Use the gcloud command-line tool to create your Pub/Sub topic. Name the topic transactions.
    
    gcloud pubsub topics create transactions

Replace project-id with your project ID.

## Create a BigQuery dataset and table

In the BigQuery web UI, create a BigQuery dataset. A BigQuery dataset is a top-level container used to contain your tables. BigQuery tables must belong to a dataset.

In the left navigation panel, click your project ID. On the right side of the window, click Create dataset.

![](images/step2.png)

In the Create dataset panel that opens on the right, enter the Dataset ID dataflow_sql_dataset. Click Create dataset.

![](images/step3.png)

## Create a BigQuery table.

In the left navigation panel of the Dataflow SQL UI, under your project, click the dataflow_sql_dataset dataset you created.
On the right side of the window, click Create table.

![](images/step4.png)

In the Create table panel that opens on the right:

In the Create table from drop-down list, select Upload.

For Select file, click Browse and choose your us_state_salesregions.csv file.

In the Table name box, enter us_state_salesregions.

Under Schema, select the Auto detect checkbox.

Click Create table.

![](images/step5.png)

In the left navigation panel, under your project, under your dataset, click us_state_salesregions. 

Under Schema, you can view the schema that was auto-generated. Under Preview, you can see the table data.

Preview the table.

![](images/step6.png)

## Find Pub/Sub sources


In the BigQuery web UI, follow these steps to switch to the Dataflow UI.

Click the More drop-down menu and select Query settings.

![](images/step7.png)


In the Query settings menu that opens on the right, select Dataflow engine.

If your project does not have the Dataflow and Data Catalog APIs enabled, you will be prompted to enable them. Click Enable APIs. Enabling the Dataflow and Data Catalog APIs might take a few minutes.

When enabling the APIs is complete, click Save.

![](images/step8.png)

The Dataflow SQL UI provides a way to find Pub/Sub data source objects for any project you have access to, so you don't have to remember their full names.

For the example in this lab, add the transactions Pub/Sub topic that you created:

In the left navigation panel, click the Add data drop-down list and select Cloud Dataflow sources.

![](images/step9.png)

In the Add Cloud Dataflow source panel that opens on the right, choose Pub/Sub topics. In the search box, search for transactions. Select the topic and click Add.

![](images/step10.png)

Assign a schema to your Pub/Sub topic

Assigning a schema lets you run SQL queries on your Pub/Sub topic data. Currently, Dataflow SQL expects messages in Pub/Sub topics to be serialized in JSON format. Support for other formats such as Avro will be added in the future.

After adding the example Pub/Sub topic as a Dataflow source, complete the following steps to assign a schema to the topic in the Dataflow SQL UI:

Select the topic in the Resources panel.

In the Schema tab, click Edit schema. The Schema side panel opens on the right.

![](images/step11.png)

The side panel in which to add or edit a schema

Toggle the Edit as text button and paste the following inline schema into the editor. Then, click Submit.

```json
    [
      {
          "description": "Pub/Sub event timestamp",
          "name": "event_timestamp",
          "mode": "REQUIRED",
          "type": "TIMESTAMP"
      },
      {
          "description": "Transaction time string",
          "name": "tr_time_str",
          "type": "STRING"
      },
      {
          "description": "First name",
          "name": "first_name",
          "type": "STRING"
      },
      {
          "description": "Last name",
          "name": "last_name",
          "type": "STRING"
      },
      {
          "description": "City",
          "name": "city",
          "type": "STRING"
      },
      {
          "description": "State",
          "name": "state",
          "type": "STRING"
      },
      {
          "description": "Product",
          "name": "product",
          "type": "STRING"
      },
      {
          "description": "Amount of transaction",
          "name": "amount",
          "type": "FLOAT64"
      }
    ]
```

![](images/step12.png)


View the schema

In the left navigation panel of the Dataflow SQL UI, click Cloud Dataflow sources.

Click Pub/Sub topics.

Click transactions.

Under Schema, you can view the schema you assigned to the transactions Pub/Sub topic.

![](images/step13.png)

## Publish messages to your Pub/Sub topic

Before you execute your SQL query, run the transactions_injector.py Python script in a command-line window in your local computer. 

The script periodically publishes messages to your Pub/Sub topic, transactions, and the script will continue to publish messages to your topic until you stop the script.

Replace project-id with your project ID in transactions_injector.py then run :

    python transactions_injector.py

## Create a SQL query

The Dataflow SQL UI lets you create SQL queries to run your Dataflow jobs.

The following SQL query is a data enrichment query. It adds an additional field, sales_region, to the Pub/Sub stream of events (transactions), using a BigQuery table (us_state_salesregions) that maps states to sales regions.

Copy and paste the following SQL query into the Query editor. Replace project-id with your project ID.

    SELECT tr.*, sr.sales_region
    FROM pubsub.topic.`project-id`.transactions as tr
      INNER JOIN bigquery.table.`project-id`.dataflow_sql_dataset.us_state_salesregions AS sr
      ON tr.state = sr.state_code

When you enter a query in the Dataflow SQL UI, the query validator verifies the query syntax. A green check mark icon is displayed if the query is valid. If the query is invalid, a red exclamation point icon is displayed. If your query syntax is invalid, clicking on the validator icon provides information about what you need to fix.

The following screenshot shows the valid query in the Query editor. The validator displays a green check mark.

Enter your query in the editor.

Below the Query editor, click Create Dataflow job.

![](images/step14.png)

In the Create Dataflow job panel that opens on the right, change the default Table name to dfsqltable_sales.

![](images/step15.png)

Optional: Dataflow automatically chooses the settings that are optimal for your Dataflow SQL job, but you can expand the Optional parameters menu to manually specify the following pipeline options:

* Maximum number of workers
* Zone
* Service account email
* Machine type
* Additional experiments
* Worker IP address configuration
* Network
* Subnetwork

Click Create. Your Dataflow job will take a few minutes to start running.

Under Job information, click the Job ID link. This opens a new browser tab with the Dataflow Job Details page in the Dataflow web UI.

![](images/step16.png)

View the Dataflow job and output

Dataflow turns your SQL query into an Apache Beam pipeline. In the Dataflow web UI that opened in a new browser tab, you can see a graphical representation of your pipeline.

![](images/step17.png)

You can click the boxes to see a breakdown of the transformations occurring in the pipeline. For example, if you click the top box in the graphical representation, labeled Run SQL Query, a graphic appears that shows the operations taking place behind the scenes.

The top two boxes represent the two inputs you joined: the Pub/Sub topic, transactions, and the BigQuery table, us_state_salesregions.

To view the output table that contains the job results, go back to the browser tab with the Dataflow SQL UI. In the left navigation panel, under your project, click the dataflow_sql_dataset dataset you created. Then, click on the output table, dfsqltable_sales. The Preview tab displays the contents of the output table.

![](images/step18.png)

View past jobs and edit your queries
The Dataflow SQL UI stores past jobs and queries in the Job history panel. Jobs are listed by the day the job started. The job list first displays days that contain running jobs. Then, the list displays days with no running jobs.

You can use the job history list to edit previous SQL queries and run new Dataflow jobs. For example, you want to modify your query to aggregate sales by sales region every 15 seconds. Use the Job history panel to access the running job that you started earlier in the tutorial, change the SQL query, and run another job with the modified query.

In the left navigation panel, click Job history.

Under Job history, click Cloud Dataflow. All past jobs for your project appear.


Click on the job you want to edit. Click Open in query editor.

![](images/step19.png)

![](images/step20.png)



Edit your SQL query in the Query editor to add tumbling windows. Replace project-id with your project ID if you copy the following query.

     SELECT
       sr.sales_region,
       TUMBLE_START("INTERVAL 15 SECOND") AS period_start,
       SUM(tr.amount) as amount
     FROM pubsub.topic.`project-id`.transactions AS tr
       INNER JOIN bigquery.table.`project-id`.dataflow_sql_dataset.us_state_salesregions AS sr
       ON tr.state = sr.state_code
     GROUP BY
       sr.sales_region,
       TUMBLE(tr.event_timestamp, "INTERVAL 15 SECOND")

Below the Query editor, click Create Cloud Dataflow job to create a new job with the modified query.

## Clean up

Don't forget to delete your Dataflow jobs, your Bigquery dataset and your Pub/Sub topic