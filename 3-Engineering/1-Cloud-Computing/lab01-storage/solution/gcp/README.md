# Storage - Lab


##  Cloud Storage

Digital objects — images, videos, documents, etc. — are a key aspect of any type of computing, whether local or cloud-based. 
Cloud Storage is a central facility for storing and retrieving your binary objects and 
is therefore utilized by the vast majority of applications on a cloud platform. 
In this hands-on lab, you’ll gain practical experience in creating and managing the primary component of Storage in the Cloud, 
the bucket, as well as manipulating the objects it holds, through both the console and the command line.

## Challenge scenario

Your company is ready to launch a brand new product! Because you are entering a totally new space, 
you have decided to deploy a new static website as part of the product launch. The new site is complete, 
but the person who built the new site left the company before they could deploy it.

## Your challenge

Your challenge is to deploy the site in the public cloud by completing the tasks below. 
You will use a the website in the directory static-website as a placeholder for the new site in this exercise. Good luck!

## Create a bucket

Create a bucket, name it with a globally unique name.

On GCS console page click on the "Create Bucket" button

![](images/step1.png)


Choose a globally unique name for the bucket, click Continue

![](images/step2.png)

Choose a Regional storage in Belgium, click Continue

![](images/step3.png)

Choose a standard storage class, click Continue

![](images/step4.png)

Choose a fine-grained access control, click Continue

![](images/step5.png)

Click Create

![](images/step6.png)

Congrats! Here is your newly created bucket!

![](images/step7.png)

## Host a the static website

Upload the static website code available at https://github.com/mblanc/website-template in your newly created bucket.

In your newly created bucket drag and drop the content of the static-website

Your files has been uploaded to your bucket!

![](images/step7-2.png)

Click on the index.html file, and try to reach the object URL in a new tab.
The file is not publicly available!

![](images/step8.png)


## Share the file

Modify the bucket and file permissions to allow anyone to access all objects.

Modify the Permissions of the bucket, click on "Add Members"

![](images/step9.png)

Add the member "AllUsers" with the role "Storage Object Viewer"

![](images/step10.png)


![step18]

## Test the static website

Try to access the static website publicly by reaching the public URL of the index.html in your bucket.

Click on the index.html file, and try to reach the object URL in a new tab.

![](images/step11.png)


![](images/step19.png)


## Enable object versioning

Try to enable versioning in your bucket and upload a new version of the static website.

Try to open both versions of the website in a web browser.


You can't do that with the Console :


see : https://cloud.google.com/storage/docs/using-object-versioning

you should use the commands :

    gsutil versioning set on gs://[BUCKET_NAME]

Modify locally the index.html file and upload the new version to your bucket.

You should see 2 versions of the file in GCS with the command :

    gsutil ls -a gs://[BUCKET_NAME]/index.html


The public URL point to the latest version (beware of Cache!).


## Delete the bucket

Don't forget to delete the bucket once you're done with the lab!


![](images/step12.png)

## Bonus

Try to redo this exercise using only the CLI for your favorite cloud platform.

```bash
# replace <bucket-id> with an globally unique bucket name
export BUCKET=<bucket-id>

# create the bucket
gsutil mb gs://$BUCKET

# copy the static website into the bucket
gsutil cp -r static-website gs://$BUCKET/


# make the file publicly available
# see https://cloud.google.com/storage/docs/access-control/making-data-public
# for all objects :

gsutil acl ch -r -u AllUsers:R gs://$BUCKET/static-website/


# enable Versioning
gsutil versioning set on gs://$BUCKET

# delete the bucket
gsutil rm -r gs://$BUCKET 
```




