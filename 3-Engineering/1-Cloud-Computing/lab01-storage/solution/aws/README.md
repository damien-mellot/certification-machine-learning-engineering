# Storage - Lab


##  Cloud Storage

Digital objects — images, videos, documents, etc. — are a key aspect of any type of computing, whether local or cloud-based. 
Cloud Storage is a central facility for storing and retrieving your binary objects and 
is therefore utilized by the vast majority of applications on a cloud platform. 
In this hands-on lab, you’ll gain practical experience in creating and managing the primary component of Storage in the Cloud, 
the bucket, as well as manipulating the objects it holds, through both the console and the command line.

## Challenge scenario

Your company is ready to launch a brand new product! Because you are entering a totally new space, 
you have decided to deploy a new static website as part of the product launch. The new site is complete, 
but the person who built the new site left the company before they could deploy it.

## Your challenge

Your challenge is to deploy the site in the public cloud by completing the tasks below. 
You will use a the website in the directory static-website as a placeholder for the new site in this exercise. Good luck!

## Create a bucket

Create a bucket, name it with a globally unique name.

On S3 console page click on the "Create Bucket" button

![step1]

Choose a globally unique name for the bucket, click Next

![step2]

Inspect the configuration options, click Next

![step3]

Inspect the permissions, click Next

![step4]

Review and click on the "Create Bucket" button

![step5]

Congrats! Here is your newly created bucket!

![step6]


[step1]: images/step1.png
[step2]: images/step2.png
[step3]: images/step3.png
[step4]: images/step4.png
[step5]: images/step5.png
[step6]: images/step6.png

## Host a the static website

Upload the static website code available at https://github.com/mblanc/website-template in your newly created bucket.

In your newly created bucket click on the "Upload" button

![step7]

Drag and drop the content of the static-website folder onto the wizard. Click on Next.

![step8]

Inspect the permissions, click Next

![step9]

Inspect the properties, click Next

![step10]

Review and click on the "Upload" button

![step11]

Your files has been uploaded to your bucket!

![step12]

Click on the index.html file, and try to reach the object URL in a new tab.

![step13]

The file is not publicly available!

![step14]

[step7]: images/step7.png
[step8]: images/step8.png
[step9]: images/step9.png
[step10]: images/step10.png
[step11]: images/step11.png
[step12]: images/step12.png
[step13]: images/step13.png
[step14]: images/step14.png


## Share the file

Modify the bucket and file permissions to allow anyone to access all objects.

Modify the Permissions of the bucket

![step15]

Edit the permissions and uncheck the Block all public access policy

![step16]

![step17]

Select all the files in your bucket and click on "Actions > Make Public"

![step18]


[step15]: images/step15.png
[step16]: images/step16.png
[step17]: images/step17.png
[step18]: images/step18.png

## Test the static website

Try to access the static website publicly by reaching the public URL of the index.html in your bucket.

Click on the index.html file, and try to reach the object URL in a new tab.

![step13]

![step19]

[step19]: images/step19.png

## Enable object versioning

Try to enable versioning in your bucket and upload a new version of the static website.

Try to open both versions of the website in a web browser.


Modify the Properties of the bucket to enable versioning.

![step20]

Modify locally the index.html file and upload the new version to your bucket.
Don't forget to make the file publicly available!

You should see 2 versions of the file in S3 :

![step21]

The public URL point to the latest version :

![step22]


[step20]: images/step20.png
[step21]: images/step21.png
[step22]: images/step22.png

## Delete the bucket

Don't forget to delete the bucket once you're done with the lab!


![step23]

[step23]: images/step23.png

## Bonus

Try to redo this exercise using only the CLI for your favorite cloud platform.

```bash
# replace <bucket-id> with an globally unique bucket name
export BUCKET=<bucket-id>

# create the bucket
aws s3 mb s3://$BUCKET

# copy the static website into the bucket
aws s3 cp website s3://$BUCKET --recursive


# make the file publicly available
# see https://aws.amazon.com/premiumsupport/knowledge-center/read-access-objects-s3-bucket/
# for all objects :

aws s3api put-object-acl --bucket $BUCKET --key <object> --acl public-read


# enable Versioning
aws s3api put-bucket-versioning --bucket $BUCKET --versioning-configuration Status=Enabled

# delete the bucket
aws s3 rb s3://$BUCKET --force 
```




