# Storage - Lab

##  Cloud Compute

## Challenge scenario

You have been asked to test the possibility for Data Scientists in your team to use cloud compute instances as workstations.

## Your challenge

Your challenge is to launch an vm instance in the cloud and connect to a jupyter notebook running on this instance from your computer.

A data scientist has provided a test notebook "csv.ipynb".

The dataset "train.csv" and "eval.csv" will be stored on a cloud storage bucket.

# Create a Cloud Storage Bucket

And upload the dataset "eval.csv" and "train.csv"

## Create a compute instance

Create a vm instance to host a jupyter server.
Your VM should be able to access a bucket in the cloud storage service.
You should be able to connect to your instance in ssh and access the port 8888 with TCP.

(hint for AWS : 
- create an IAM Role allowing EC2 instances to access files in a S3 bucket
- create a SecurityGroup allow tcp connection on port 22 and 8888 for protocol TCP from Source 0.0.0.0/0)

(hint for GCP : 
- create an Firewall Rules allowing ssh connections on port 22 and TCP connections on port 8888 for network-tag "allow-jupyter"
- add the network tag "allow-jupyter" to your instance)

## Host a jupyter server

Connect to your instance with ssh and install a jupyter server.

Here is the procedure to install jupyter on a new machine :

```bash
# install Miniconda as your python distribution : 
wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh -O ~/miniconda.sh
bash ~/miniconda.sh

# activate the base conda env
conda activate

# install python librairies needed e.g.:
pip install tensorflow --no-cache-dir
pip install matplotlib
pip install pandas
pip install pillow
pip install jupyterlab

# for AWS
pip install boto3

# for GCP
pip install google-cloud-storage

# launch jupyter on port 8888 open to external connections
# (see https://jupyter-notebook.readthedocs.io/en/stable/public_server.html#notebook-public-server) :
jupyter notebook --port=8888 --no-browser --ip=0.0.0.0 &
```

## Test the jupyter notebook

Try to access the jupyter notebook from your browser.
http://<INSTANCE-EXTERNAL-IP>:8888/?token=<TOKEN>

(The token is displayed in your ssh console when launching jupyter)

Visit http://<INSTANCE-EXTERNAL-IP>:8888/lab for JupyterLab

Drag and drop "csv.ipynb" in Jupyter Lab file browser.

Execute all cells from the notebook.
Replace BUCKET_NAME with the name of your bucket.

## Bonus

Try to redo this exercise using only the CLI for your favorite cloud platform.

## Delete the bucket and the compute instance!

Don't forget to delete the bucket and the compute instance once you're done with the lab!
