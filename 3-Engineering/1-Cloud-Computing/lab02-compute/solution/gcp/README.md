# Storage - Lab

##  Cloud Compute

## Challenge scenario

You have been asked to test the possibility for Data Scientists in your team to use cloud compute instances as workstations.

## Your challenge

Your challenge is to launch an vm instance in the cloud and connect to a jupyter notebook running on this instance from your computer.

A data scientist has provided a test notebook "csv.ipynb".

The dataset "train.csv" and "eval.csv" will be stored on a cloud storage bucket.

# Create a Cloud Storage Bucket

And upload the dataset "eval.csv" and "train.csv"

## Create a compute instance

Create a vm instance to host a jupyter server.
Your VM should be able to access a bucket in the cloud storage service.
You should be able to connect to your instance in ssh and access the port 8888 with TCP.

(hint for AWS : 
- create an IAM Role allowing EC2 instances to access files in a S3 bucket
- create a SecurityGroup allow tcp connection on port 22 and 8888 for protocol TCP from Source 0.0.0.0/0)

(hint for GCP : 
- create an Firewall Rules allowing ssh connections on port 22 and TCP connections on port 8888 for network-tag "allow-jupyter"
- add the network tag "allow-jupyter" to your instance)

From GCE service click on "Create Instance"

![](images/step1.png)

Choose an instance in Belgium

![](images/step2.png)

Click on Advanced Settings > Networking and add a network tag

![](images/step3.png)

Launch your instance

![](images/step4.png)


Navigate to VPC Network > Firewall Rules
![](images/step7.png)


Create a new Firewall rules
![](images/step8.png)

Add a rule for tag "jupyter" from source ip ranges "0.0.0.0/0" and port 8888 for protocol TCP
![](images/step9.png)


## Host a jupyter server

Connect to your instance with ssh and install a jupyter server.

Connect to your instance with ssh on a new browser tab

![](images/step5.png)

Here is the procedure to install jupyter on a new machine :

```bash
# install Miniconda as your python distribution : 
wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh -O ~/miniconda.sh
bash ~/miniconda.sh

# activate the base conda env
conda activate

# install python librairies needed e.g.:
pip install tensorflow --no-cache-dir
pip install matplotlib
pip install pandas
pip install pillow
pip install jupyterlab

# for AWS
pip install boto3

# for GCP
pip install google-cloud-storage


# grab a notebook from the internet as a test
wget https://raw.githubusercontent.com/tensorflow/docs/master/site/en/tutorials/keras/classification.ipynb

# launch jupyter on port 8888 open to external connections
# (see https://jupyter-notebook.readthedocs.io/en/stable/public_server.html#notebook-public-server) :
jupyter notebook --port=8888 --no-browser --ip=0.0.0.0 &
```

## Test the jupyter notebook


Try to access the jupyter notebook from your browser.
http://<INSTANCE-EXTERNAL-IP>:8888/?token=<TOKEN>

Click on the instance name in GCE VM Instances page and note the External IP.
![](images/step6.png)

![](images/step10.png)


(The token is displayed in your ssh console when launching jupyter)

Visit http://<INSTANCE-EXTERNAL-IP>:8888/lab for JupyterLab

Drag and drop "csv.ipynb" in Jupyter Lab file browser.

Execute all cells from the notebook.
Replace BUCKET_NAME with the name of your bucket.

![](images/step11.png)

## Bonus

Try to redo this exercise using only the CLI for your favorite cloud platform.

```bash
# default zone in Belgium
gcloud config set compute/zone europe-west1-b

# Create a firewall rule
gcloud compute firewall-rules create allow-jupyter --allow=tcp:8888 --target-tags=jupyter 

# Launch a vm instance
gcloud compute instances create "my-vm-2" \
--machine-type "n1-standard-1" \
--image-project "debian-cloud" \
--image "debian-9-stretch-v20190213" \
--subnet "default"
--tags=jupyter

# Connect to your vm instance
gcloud compute ssh my-vm

# get instance external ip
gcloud compute instances describe my-vm

# Delete your instance
gcloud compute instances delete my-vm
```

## Delete the bucket and the compute instance!

Don't forget to delete the bucket and the compute instance once you're done with the lab!
