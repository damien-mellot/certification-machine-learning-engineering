## Create a S3 bucket

Your bucket should contain the word "sagemaker" in its name.

Upload the files train.csv, test.csv and valid.csv in your bucket

## Create an Amazon SageMaker Notebook Instance

An Amazon SageMaker notebook instance is a fully managed machine learning (ML) Amazon Elastic Compute Cloud (Amazon EC2) compute instance that runs the Jupyter Notebook App. 

You use the notebook instance to create and manage Jupyter notebooks that you can use to prepare and process data and to train and deploy machine learning models. 

If necessary, you can change the notebook instance settings, including the ML compute instance type, later.

To create an Amazon SageMaker notebook instance

Open the Amazon SageMaker console at (https://console.aws.amazon.com/sagemaker/).
Choose Notebook instances, then choose Create notebook instance.

![](images/step1.png)

![](images/step2.png)

On the Create notebook instance page, provide the following information (if a field is not mentioned, leave the default values):

For Notebook instance name, type a name for your notebook instance.

For Instance type, choose ml.t2.medium. This is the least expensive instance type that notebook instances support, and it suffices for this exercise.

For IAM role, choose Create a new role, then choose Create role.

Choose Create notebook instance.

![](images/step5.png)

In a few minutes, Amazon SageMaker launches an ML compute instance—in this case, a notebook instance—and attaches an ML storage volume to it. The notebook instance has a preconfigured Jupyter notebook server and a set of Anaconda libraries.

## Create a notebook

You can create a Jupyter notebook in the notebook instance you created.

Open the notebook instance you created by choosing Open JupyterLab for JupyterLab view next to the name of the notebook instance.

![](images/step6.png)

Drag and drop the sagemaker-mnist.ipynb in the JupyterLab File Browser


## Train and Deploy the Model to Amazon SageMaker

Execute all the cells in the sagemaker-mnist.ipynb

You will be able to see the different resources you will create from the notebook in the Sagemaker console.
Enter the name of your S3 bucket in the first cell.

The training job

![](images/step7.png)

The model

![](images/step8.png)

The endpoint configuration

![](images/step9.png)

The endpoint

![](images/step10.png)


## Clean Up

Open the Amazon SageMaker console at https://console.aws.amazon.com/sagemaker/ and delete the following resources:

The endpoint. Deleting the endpoint also deletes the ML compute instance or instances that support it.

The endpoint configuration.

The model.

The notebook instance. Before deleting the notebook instance, stop it.

Open the Amazon S3 console at https://console.aws.amazon.com/s3/ and delete the bucket that you created for storing model artifacts and the training dataset.