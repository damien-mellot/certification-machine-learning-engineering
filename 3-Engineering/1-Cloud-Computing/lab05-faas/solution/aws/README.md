
To be able able to train a Convolutional Neural Network on images we need to have images of the same size.
We you want to resize each images uploaded to a cloud bucket storage.
Your challenge is to create a serverless function that will be invoked eache time an image is uploaded.
Your function will dowload the image (in the /tmp directory), resize the image and upload the new version in another bucket.


## Create two cloud storage bucket

ex : fadazdazd-lab-faas-raw and fadazdazd-lab-faas-resized

## Create the Execution Role

1. Open the roles page in the IAM console.
2. Choose Create role.
3. Create a role with the following properties.
    * Trusted entity – AWS Lambda.
    * Permissions – AWSLambdaExecute.
    * Role name – lambda-s3-role.

## Create a cloud function

Your function will download the image (in the /tmp directory), resize the image and upload the new version in another bucket.

see the function in aws_lambda_resize_image/main.py

You should package your function in a ec2 t2.micro instance with an Amazon Linux 2 AMI!!!!

Create the ec2 instance.

Upload the main.py and requirements.txt file

    scp -i "<path to pem file>" main.py ec2-user@ecX-XX-XXX-XXX-XX.eu-west-1.compute.amazonaws.com:~
    scp -i "<path to pem file>" requirements.txt ec2-user@ecX-XX-XXX-XXX-XX.eu-west-1.compute.amazonaws.com:~
    
Connect to your instance with ssh

    ssh -i "<path to pem file>" ec2-user@ecX-XX-XXX-XXX-XX.eu-west-1.compute.amazonaws.com
    
    
In your ec2 instance install python3

    sudo yum install python3 -y
    
Create a virtual environment and activate it:

    python3 -m venv venv
    source venv/bin/activate
    
Install dependencies:

    pip3 install -r requirements.txt
    
Package your function:

    cd venv/lib/python3.7/site-packages/
    zip -r9 ~/function.zip .
    cd ~
    zip -g function.zip main.py
    
In your local computer download the packaged function:

    scp -i "<path to pem file>" ec2-user@ecX-XX-XXX-XXX-XX.eu-west-1.compute.amazonaws.com:function.zip .
    
## Publish your function

In the AWS Lambda Console click Create Function.

![](images/step1.png)

Name your function and choose a Python 3.7 Runtime.

![](images/step2.png)

Use an existing role, choose the lambda-s3-role. Click Create Function.

![](images/step3.png)

In the function UI click Add Trigger.

![](images/step4.png)

Choose S3 and select your bucket. Click Add.

![](images/step5.png)

Upload your zip file, and enter main.lambda_function as the Handler. Click Save.

![](images/step6.png)

You can watch your function invocation and your logs from the Monitoring tab.

![](images/step7.png)

## Test your serverless function 

Upload an image to the first bucket and verify that the resized image has appeared in the second bucket.

## Clean up

Don't forget to delete the two cloud storage buckets and your function.

