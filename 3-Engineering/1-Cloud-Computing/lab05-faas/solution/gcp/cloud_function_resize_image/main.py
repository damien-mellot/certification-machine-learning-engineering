from google.cloud import storage
import uuid
from PIL import Image

# Create cloud storage client
client = storage.Client()

def resize_image(image_path, resized_path):
    with Image.open(image_path) as image:
        newsize = (300, 300)
        image = image.resize(newsize)
        image.save(resized_path)

def cloud_function_resize_image(data, context):
    ## retrieve bucket name and image object key
    bucket_name = data['bucket']
    key = data['name']

    ## Compute the download path and the path of the resized image (upload_path)
    tmpkey = key.replace('/', '')
    download_path = '/tmp/{}{}'.format(uuid.uuid4(), tmpkey)
    upload_path = '/tmp/resized-{}'.format(tmpkey)

    # Download the file to download_path
    bucket = client.get_bucket(bucket_name)
    blob = storage.Blob(key, bucket)
    blob.download_to_filename(download_path)

    # Resize the file and save it to upload_path
    resize_image(download_path, upload_path)

    # Upload the resized file to the resized bucket : bucket_name + '-resized'
    target_bucket = client.bucket(bucket_name + '-resized')
    blob = target_bucket.blob(key)
    blob.upload_from_filename(upload_path)

    # Terminate the function by returning a value (will be written to Stackdriver logs)
    return 'resized file'.format(key)