
To be able able to train a Convolutional Neural Network on images we need to have images of the same size.
We you want to resize each images uploaded to a cloud bucket storage.
Your challenge is to create a serverless function that will be invoked eache time an image is uploaded.
Your function will dowload the image (in the /tmp directory), resize the image and upload the new version in another bucket.


## Create two cloud storage bucket

ex : fadazdazd-lab-faas-raw and fadazdazd-lab-faas-resized

## Create a cloud function

Your function will download the image (in the /tmp directory), resize the image and upload the new version in another bucket.

see the function in cloud_function_resize_image/main.py

In the cloud_function_resize_image directory execute:

    zip -r9 function.zip .
    
## Publish your function

In the Google Cloud Functions Console click Create Function.

![](images/step1.png)

Choose a name. Select Cloud Storage for the Trigger. Select your bucket. Upload your zip file. Choose a Python3.7 Runtime.

![](images/step2.png)

The function to execute is named cloud_function_resize_image. Click Create.

![](images/step3.png)

Click on your function once created.

![](images/step4.png)

You can watch your function invocation and your function Console.

![](images/step5.png)

## Test your serverless function 

Upload an image to the first bucket and verify that the resized image has appeared in the second bucket

## Clean up

Don't forget to delete the two cloud storage buckets and your function.

