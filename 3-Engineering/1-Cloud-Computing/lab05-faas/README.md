
To be able able to train a Convolutional Neural Network on images we need to have images of the same size.
We you want to resize each images uploaded to a cloud bucket storage.
Your challenge is to create a serverless function that will be invoked eache time an image is uploaded.
Your function will dowload the image (in the /tmp directory), resize the image and upload the new version in another bucket.

## Create two cloud storage bucket

ex : fadazdazd-lab-faas-raw and fadazdazd-lab-faas-resized

## Create a cloud function

Your function will download the image (in the /tmp directory), resize the image and upload the new version in another bucket.

## Package your function

Create a zip file containing your function and a requirements.txt file. (GCP)

For AWS check https://docs.aws.amazon.com/lambda/latest/dg/python-package.html#python-package-dependencies

## Publish your function

Create your function in the Cloud Console.

## Test your serverless function 

Upload an image to the first bucket and verify that the resized image has appeared in the second bucket

## Clean up

Don't forget to delete the two cloud storage buckets and your function.

