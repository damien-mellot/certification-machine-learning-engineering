import boto3
import uuid
from urllib.parse import unquote_plus
from PIL import Image
import PIL.Image

# TODO Create a storage client

def resize_image(image_path, resized_path):
    with Image.open(image_path) as image:
        image.thumbnail(tuple(x / 2 for x in image.size))
        image.save(resized_path)

def lambda_function(event, context):
    for record in event['Records']:
        ## TODO retrieve bucket name and image object key
        bucket = record['s3']['bucket']['name']
        key = unquote_plus(record['s3']['object']['key'])


        ## Compute the download path and the path of the resized image (upload_path)
        tmpkey = key.replace('/', '')
        download_path = '/tmp/{}{}'.format(uuid.uuid4(), tmpkey)
        upload_path = '/tmp/resized-{}'.format(tmpkey)

        # TODO Download the file to download_path

        # Resize the file and save it to upload_path
        resize_image(download_path, upload_path)

        # TODO Upload the resized file to the resized bucket : bucket_name + '-resized'