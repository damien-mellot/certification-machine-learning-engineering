# Copyright 2017 Google Inc. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from datetime import datetime
from dataclasses import dataclass
from typing import List
import logging
import os
import boto3

from flask import Flask, redirect, render_template, request

from google.cloud import storage
from google.cloud import vision


CLOUD_STORAGE_BUCKET = os.environ.get('CLOUD_STORAGE_BUCKET')
S3_BUCKET = os.environ.get('S3_BUCKET')

@dataclass
class Image:
    '''Class for keeping track of an item in inventory.'''
    blob_name: str
    image_public_url: str
    gcp_labels: List[str]
    rek_labels: List[str]


app = Flask(__name__)


@app.route('/')
def index():
    # Return a Jinja2 HTML template and pass in image_entities as a parameter.
    return render_template('index.html', image_entity={})


@app.route('/upload_photo', methods=['GET', 'POST'])
def upload_photo():
    photo = request.files['file']
    image_bytes = photo.read()

    # TODO : upload the file to your Cloud Storage bucket and make the image public.
    # see https://cloud.google.com/storage/docs/uploading-objects#storage-upload-object-code-sample
    # look for upload_from_string here : https://googleapis.dev/python/storage/latest/blobs.html
    # see https://cloud.google.com/storage/docs/access-control/making-data-public#storage-make-object-public-python

    # TODO : call the vision API to get labels from the image
    # see https://cloud.google.com/vision/docs/labels

    gcp_labels = []

    # TODO : upload the file to your S3 bucket and make it publicly available.
    # see : https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/s3.html#client
    # see : https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/s3.html#S3.Client.put_object

    # TODO : call the AWS Rekognition API to get labels from the image.
    # see : https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/rekognition.html
    # see : https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/rekognition.html#Rekognition.Client.detect_labels
    rek_labels = []

    # Construct the new entity using the key. Set dictionary values for entity
    # keys blob_name, storage_public_url, timestamp, and joy.
    entity = Image(blob.name, blob.public_url, gcp_labels, rek_labels)

    # Redirect to the home page.
    return render_template('index.html', image_entity=entity)


@app.errorhandler(500)
def server_error(e):
    logging.exception('An error occurred during a request.')
    return """
    An internal error occurred: <pre>{}</pre>
    See logs for full stacktrace.
    """.format(e), 500


if __name__ == '__main__':
    # This is used when running locally. Gunicorn is used to run the
    # application on Google App Engine. See entrypoint in app.yaml.
    app.run(host='127.0.0.1', port=8080, debug=True)
