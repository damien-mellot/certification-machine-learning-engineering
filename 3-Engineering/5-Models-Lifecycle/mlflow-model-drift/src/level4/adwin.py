
import pandas as pd
from concept_drift import AdWin
import mlflow.pyfunc
import matplotlib.pyplot as plt

# use basic logging, logs from the producer should not be present in the producer, only in the consumer
import mlflow
import warnings
from collections import defaultdict


class MLFlowAdwin(mlflow.pyfunc.PythonModel):
    """
    see doc here
    https://mlflow.org/docs/latest/models.html#custom-python-models
    """
    def __init__(self, feature_name, *args, **kwargs):
        self.feature_name = feature_name
        self.detector = AdWin(*args, **kwargs)

    def predict(self, context, model_input: pd.DataFrame):
        values = list()
        if self.feature_name not in model_input:
            warnings.warn(
                '{} not in input columns, skipping'.format(self.feature_name)
            )
            return pd.Series([])

        key = '{} drift'.format(self.feature_name)
        feature = model_input[self.feature_name]
        for idx, e in enumerate(feature):
            change = self.detector.set_input(e)
            values.append(change)

        s = pd.Series(values, index=model_input.index).astype(float)

        with mlflow.start_run():
            plt.plot(feature.index, feature.values)
            true_df = s[s == 1.0]
            for datetime in true_df.index:
                plt.axvplot(x=k, color='red', linestyle='--')
            pyplot.savefig('adwin_plot.svg')
            mlflow.log_artifact('adwin_plot.svg')

        return s

    def __repr__(self):
        return "ADWIN on feature : {}".format(self.feature_name)

