# Level 4

## Visualization
This level is made to provide a visualization layer on our detectors.

MLFflow provides a [log_artifact](https://www.mlflow.org/docs/latest/python_api/mlflow.html#mlflow.log_artifact) method.


-------------------------
### To complete this level
- modify `adwin.py` to create a plot and save it using `mlflow.log_artifact`
- run `mlflow ui` and check you can cisualize drifts


