# Level 3

### We did something wrong ...
Look at the code in `level2/adwin.py`, we keep a *defaultdict*, it a new feature comes in, our
*defaultdict* automatically instantiate a new detector.

Now:
 - imagine we throw an error for a given feature: all the code will crash ...
 - instantiation is based on the name of the feature. If the same feature comes with a different name ...
 - features are grouped together. I big organizations, many models might use the same feature.

This makes our service not maintainable on the long term.
Picture a big organization : many models may use the same variable. If a variable drifts, 
teams using this feature to serve predictions should be informed.

Formally, there is a one-to-many relationship between models and features
Models are depedant on features. Triggering an alarm on a single feature may need adaptation on many models.

---------------------
### To complete this level
- [ ] update `create_detector.py` so we instantiate a model for a fixed variable name
- [ ] try it !  `mlflow run . -e create_drift_detector -P output_uri=... -P feature_name=...`
- [ ] try running the same command with some wrong model paramets. Do you get an error ? eg. `mlflow run . -e create_drift_detector -P output_uri=../models/level2/ -P feature_name='feature1' -P model_params='{"random_param" : 3}'`
- [ ] make predictions : `mlflow models predict -m models/your_model -i data/elecNormNew.csv`

```bash
mlflow run . -e create_drift_detector_level_3 -P output_uri=../models/victorian_price -P feature_name='victorian_price'
```

------------------
### Key learnings
Now you see every detector is responsible for a feature !!! You demonstrated a truly micro-service mindset. Brilliant.
