"""
Instantiate a custom mlflow.pyfunc.PythonModel and save it for later use
"""

import click

import mlflow.pyfunc

from collections import defaultdict

from .adwin import MLFlowAdwin

import os
CURR_DIR = os.path.dirname(os.path.abspath(__file__))

import json


@click.command()
@click.option('--output-uri', '-o', required=True)
@click.option('--feature-name', '-n', required=True)
@click.option('--model-params', '-p', default="{}")
def create(output_uri, feature_name, model_params):
    kwargs = json.loads(model_params)
    model = MLFlowAdwin(feature_name=feature_name, **kwargs)

    mlflow.pyfunc.save_model(
        # TODO : choose arguments carefully
    )

if __name__ == '__main__':
    create()
