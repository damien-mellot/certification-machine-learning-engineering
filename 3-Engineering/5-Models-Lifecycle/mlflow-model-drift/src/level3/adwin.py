
import pandas as pd
from concept_drift import AdWin
import mlflow.pyfunc

# use basic logging, logs from the producer should not be present in the producer, only in the consumer
import mlflow
import warnings
from collections import defaultdict


class MLFlowAdwin(mlflow.pyfunc.PythonModel):
    """
    see doc here
    https://mlflow.org/docs/latest/models.html#custom-python-models
    """
    def __init__(self, feature_name, *args, **kwargs):
        self.feature_name = feature_name
        self.detector = AdWin(*args, **kwargs)

    def predict(self, context, model_input: pd.DataFrame):
        """
        mlflow models only take pandas.DataFrame as input
        In our case we need to process a pd.Series
        So we take a DataFrame and immediately get a specific column
        """
        res = list()
        if self.feature_name not in model_input:
            warnings.warn(
                '{} not in input columns, skipping'.format(self.feature_name)
            )
            return pd.Series([])
        s = model_input[self.feature_name]
        for idx, e in enumerate(s):
            # 1. call `set_input` on the current entry to update `self.detector`
            # 2. save current result into `res`
            pass 
        return pd.Series(res)

    def __repr__(self):
        # NOTE : __repr__ method is now specific to a single feature !!!
        return "ADWIN on feature : {}".format(self.feature_name)

