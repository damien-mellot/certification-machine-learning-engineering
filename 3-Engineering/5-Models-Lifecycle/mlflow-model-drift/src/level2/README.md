# Level 2

### What can we do better ?
In level 1 we needed to run a python interpreter to instantiate our custom model and save it somewhere.

This is OK for a first draft, but if we want to provide a **ready-to-use** service we have to show our -yet imaginary- end users the way.
We will operate by implementing a **Command Line Interface**, and make it (again) fully compatible with your new favourite tool : MLFLow.

This CLI has one ultimate goal : **Instantiate a drift detector, make it ready to use, and save it**.

---------------------
### To complete this level
- [ ] change the appropriate files to make mlflow properly install the code in `../concept_drift` : we don't want local imports in production. A remote version is available here : https://github.com/remiadon/concept-drift.git@setup
- [ ] fill `create_detector.py` in order to properly instantiate a detector. You should be able to run this python script (appart from MLFlow), like so : `python -m level2.create_detector --ouput-uri your_model_path`
- [ ] go to `MLProject` and check the corresponding **entry point**
- [ ] call `mlflow run` with some parameters, see [the doc](https://www.mlflow.org/docs/latest/projects.html#running-projects)

------------------
### Key learnings
Saving models using MLFLow API allows us to provide a real user-oriented service.

In big organizations, users may be other colleagues from other services (who knows ? Data Engineers ?)
The complexity should be hidden from them : as developpers we provide well-documented APIs, ready-to-use.
