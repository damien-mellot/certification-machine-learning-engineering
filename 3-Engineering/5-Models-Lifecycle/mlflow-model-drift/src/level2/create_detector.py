"""
Instantiate a custom mlflow.pyfunc.PythonModel and save it for later use
"""

import click

import mlflow.pyfunc

from .adwin import MLFlowAdwin

import os
CURR_DIR = os.path.dirname(os.path.abspath(__file__))

import json

@click.command()
@click.option('--output-uri', '-o', required=True)
def create(output_uri):
    model = MLFlowAdwin()

    mlflow.pyfunc.save_model(
        # TODO : choose arguments carefully
    )

if __name__ == '__main__':
    create()
