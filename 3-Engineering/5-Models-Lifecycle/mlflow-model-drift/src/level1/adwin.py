
import pandas as pd
from .concept_drift.adwin import AdWin
import mlflow.pyfunc

# use basic logging, logs from the producer should not be present in the producer, only in the consumer
import mlflow
import warnings
from collections import defaultdict


class MLFlowAdwin(mlflow.pyfunc.PythonModel):
    """
    see doc here
    https://mlflow.org/docs/latest/models.html#custom-python-models
    """
    def __init__(self, *args, **kwargs):
        self.detectors = defaultdict(lambda : AdWin(*args, **kwargs))

    def predict(self, context, model_input: pd.DataFrame):
        res = defaultdict(list)
        for col in model_input.columns:
            # 1. retrieve detector from `col`
            # 2. call `set_input` on the current entry to update the detector
            # 3. save current result into `res`
            pass
        return pd.DataFrame.from_dict(res)

    def __repr__(self):
        return "ADWIN"

