# Level 1

This level aims at making the first interactions between our python detectors (defined in `concept_drift/`) and MLFlow

Go to `adwin.py`. This file must define a custom MLFlow model, so that our original python class is compatible with MLFLow, and therefore can be 
 - requested via a Rest API
 - encapsulated inside an environment for better reproducibility

---------------------
### Functional definition
Many drift detectors have been conceived in the last decades, but they all rely on the same functional definition
- They analyse a single variable
- They are time-dependant (or at least order-dependant)
- They "throw" 1 values when an concept drift is encountered. You can see a drift as an alarm, we are only looking for WHEN to throw an alarm. Do we raise false alarms ? How many of them ?

------------------------
### Implementation details

Read the [MLFlow custom model example](https://www.mlflow.org/docs/latest/models.html#model-customization), especially the `AddN` class, and fill the code in `adwin.py` so you detect drifts from a given input matrix,
on a column-by-column basis.

Our custom class will use a **[python default dict](https://stackoverflow.com/questions/5900578/how-does-collections-defaultdict-work)** to hold our internal detectors. This is realy handy because everytime we don't know a feature, we instantiate a surrogate detector for this feature. We will see this has limitations, but for this level we stick with it.

MLFlow is made to transmit/receive **pandas.DataFrames** via its Rest interfaces

---------------------
### To complete this level
- [ ] add some glue code into `adwin.py`
- [ ] just like in the [Creating a custom model](https://www.mlflow.org/docs/latest/models.html#id27) example, open a python interpreter, instantiate a model, and save it via `mlflow.pyfunc.save_model`
- [ ] right after saving your model, try reloading it via `mlflow.pyfunc.load_model`
- [ ] you can try making predictions from the interpreter  #OPTIONAL
- [ ] serve the model with the `mlflow models serve` command
- [ ] adapt the [curl command from this section](https://www.mlflow.org/docs/latest/models.html#commands) and make your first predictions !!!
- [ ] make predictions via the `mlflow models predict` command, using the .csv files in `data/`
- [ ] you can try the `mlflow models build_docker` command on your model #OPTIONAL


------------------
### Key learnings
You have been doing great !! You now have a bunch of **detectors ready to ingest data from features on the fly**. You think this is only a small local example ? Look closely at at the commands we used. Our model was saved locally but it could be somewhere on the cloud, just replace the model URI at every call. 

You actually designed something on your local machine that can be ported to a cloud infrastructure with :
 - no modification on the code side
 - small modification on the API calls

This is what we are looking for !!

Also, take some time to think about the class definition in `adwin.py`. Why is there no `fit` method ? Can we make our services even more managable ?

Finally, be aware that making some external library respect a pre-defined interface is a well-known [design pattern](https://en.wikipedia.org/wiki/Software_design_pattern) in computer science, namely [Adapter](https://en.wikipedia.org/wiki/Adapter_pattern).
