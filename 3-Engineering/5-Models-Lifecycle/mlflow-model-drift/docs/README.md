## Concept drifts
first read the PDF document. This gives explanation on how ADWIN works



## Different types of drifts
<img src='drift_types.gif' width='500'/>


--------------------
---------------------
## Different use cases
Mainly two
 - **detecting change on a input variable**
 - **detecting change on a given metric**, eg. accuracy suddenly dropping for a given classifier
