## MLflow drift detection

### Intro
In this tutorial, we are going to deploy drift detectors with MLFlow.
For this we need to rethink the way we buil our workflows. While MLFlow encourages offline training of statistical models before they are exposed for predictions, we will go for a completly different way

First things first, you nead to be familiar with [MLFLow core components](https://mlflow.org/docs/latest/concepts.html#mlflow-components) before getting any further. 

In this tutorial, we will see that :
- MLFlow models can be hijacked to serve specific usecases
- MLFlow tracking is a general-purpose interface. 

#### Before starting
Make sure you install mlflow 
```bash
pip install mlflow==1.12.1
```


### Project architecture
- `data/` contains tabular data, some of the features in these data contain concept drifts. We want to detect these drifts.
- `src/` contains all the source code. Inside there are many subfolders
  - `concept_drift/` is a python package already implemented for you. **You should not modify this code**. In real situations, we are often provided pieces of code from some other team, and we only know very little about the internals of this code. Anyway, this folder contains drift detection methods to be used.
  - `level*/` folders contain the code to be filled, for each corresponding level. Make yourself at home.
- you may want to create a `models/` folder at the root to store all our amazing models


# How the existing code work
You will find some existing python code in `src/concept_drift/`. The basic usage is the following (`cd src/`)
```python
from concept_drift.adwin import AdWin

adwin = AdWin()
for i in range(1000):
    if adwin.set_input(i):
        print("Here is a drift")
```

The code is take from this repo : https://github.com/blablahaha/concept-drift

The functional definition is quite simple : we iterate over entries, and for each entry we update the internal data structure and return a boolean stating if there is a drift.
That's all you have to know for this practical session !

## Further readings
* [Adaptive Windowing](http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.144.2279&rep=rep1&type=pdf)
* [Electricity dataset description](http://citeseerx.ist.psu.edu/viewdoc/download;jsessionid=4A7541979530D9152F493D95ECEF38BE?doi=10.1.1.43.9013&rep=rep1&type=pdf), see section 2.3.2
* [How good is the Electricity benchmark for evaluating concept drift adaptation](https://arxiv.org/pdf/1301.3524v1.pdf)
* [Putting ML in productions using Kafka](https://towardsdatascience.com/putting-ml-in-production-i-using-apache-kafka-in-python-ce06b3a395c8)
