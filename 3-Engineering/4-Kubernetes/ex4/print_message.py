import os
import time
import random

i = 0
random_int = random.randint(0, 100)
folder = "/data"
filename = f"file.txt"
file_path = os.path.join(folder, filename)

while True:
    i = i+1
    message = f"coucou {i}"
    with open(file_path, "a") as f:
        f.write(message)
    time.sleep(5)
