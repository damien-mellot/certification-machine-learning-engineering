#creation order is important
kubectl apply -f pv.yml
kubectl apply -f pvc.yml
kubectl apply -f kubernetes-0-ex4.yml

kubectl exec -it kubernetes-0-ex4 -- /bin/bash
ls -lh /data
kubectl delete -f kubernetes-0-ex4.yml
kubectl apply -f kubernetes-0-ex4.yml
kubectl exec -it kubernetes-0-ex4 -- /bin/bash
ls -lh /data #new file appeared

#deletion order is important
kubectl delete -f kubernetes-0-ex4.yml
kubectl delete -f pvc.yml
kubectl delete -f pv.yml
